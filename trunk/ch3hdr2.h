﻿// -*- mode:c++; coding:utf-8-with-signature -*-

// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#include "lfloat.h"

#include <vector>
using namespace std;

#ifdef _MSC_VER 
  //not #if defined(_WIN32) || defined(_WIN64) because we have strncasecmp in mingw
  #define strncasecmp _strnicmp
  #define strcasecmp _stricmp
#endif

struct funcdef_t
{
	const char* name;		// TODO: 削除する
	void* ptr1;		// lfloat版
	int param;
};

struct vbase
{
    // vbase() { }
    // 定義しないと, 未定義エラー.
    virtual ~vbase() { }
};

struct value_t: public vbase
{
	value_t() { }
	virtual ~value_t() { }

	lfloat lf;
	double dbl;
};

struct CSymbol: public vbase
{
	CSymbol();
	virtual ~CSymbol();

	string name;
	int type;	// 0 = 未定義，1 = 変数，2 = 関数, 3 = ユーザー定義関数

	value_t v;
	funcdef_t f;

	bool operator == (const CSymbol& a) const { return !strcasecmp(name.c_str(), a.name.c_str()); }
};

////////////////////////////////////////////////////////////////////////////
// 変数，関数など

typedef vector<CSymbol> CSymbolList;
typedef vector<CSymbolList> SymbolsLine;

extern CSymbolList sl;	// TODO: 削除。SymsLineを使う。

////////////////////////////////////////////////////////////////////////////
// ガベージ・コレクター

typedef vector<vbase*> CValPtrList;

extern CValPtrList vl;

inline value_t* new_val_ptr()
{
	value_t* p = new value_t;
	vl.push_back(p);
	return p;
}

inline CSymbol* new_sym_ptr()
{
	CSymbol* p = new CSymbol;
	vl.push_back(p);
	return p;
}

////////////////////////////////////////////////////////////////////////////

typedef vector<value_t> ParamList;	// 実引数
typedef vector<CSymbol> ArgList;	// 仮引数

extern ParamList pl;
extern ArgList al;
