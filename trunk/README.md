# Q's Calc 2

コンソール型の数式電卓。高い精度と手軽な計算。


Website:
  https://www.nslabs.jp/qscalc.rhtml
  


## License

LGPLv3.
Copyright (c) 1997-1999, 2018-2023 Hisashi Horikawa. All rights reserved.




## How to build

FMA3 .. 2013年以降のCPUで使える。有効にして問題ない。
 - Intel: Haswell マイクロアーキテクチャ. 第4世代Intel Coreプロセッサ, 2013年発売。
 - AMD: Piledriver microarchitecture. 2012年頃.

個別のオプションを指定する前に, マイクロアーキテクチャレベルを指定する。`x86-64-v2` (SSE3, SSE4.2, SSSE3; not AVX) がよい。これに <kbd>-mfma</kbd> を追加すればよい。


積み残し
 - lfloat: round() がいつでもコピーしている。Swap でいいときはコピー不要.
 - Windows でのテスト
 - FreeBSD でのテスト



## `libqslargedecimal/`

### 多倍長整数ライブラリ

任意精度の整数ライブラリ。下の任意精度10進ライブラリの一部を切り出したものなので、パフォーマンスは大したことない。
除算は, とりあえず 20年前に実装した Donald Knuth's “Algorithm D” そのまま。
解説 <https://skanthak.homepage.t-online.de/division.html> ほかにもいろいろ。 


 - C++ ヘッダと plain C ヘッダがある。
 - テスト済み:
     + 四則演算, Valgrind でメモリリークがない.
 - 割り算の丸めは、0 に向かって丸める。
 - 0 除算は, 定義域エラー `std::domain_error` 例外を投げる。



### 任意精度10進浮動小数点数ライブラリ   -- ビルドは通ったがテスト未了●

通常の浮動小数点数は 2進数になっている。そのため, 0.1 を正確に表現することができない。このライブラリは 10進数で計算する。

最近のプログラミング言語では、10進数で計算する型も提供されていることが多い。Ruby の `BigDecimal` 型, C# の `decimal` 型, Python の `Decimal` 型. C言語でも, ISO/IEC TS 18661-2:2015 (C11に対する拡張) が取り込まれて, C23 から `_Decimal32`, `_Decimal64`, `_Decimal128` 型が入る。C++ では, 一足先に C++03 に対する拡張として, `std::decimal::decimal128` 型などが入っていた。

IEEE 754 の10進形式は, 次の精度を持つ:
   decimal32  7桁
   decimal64  16桁
   decimal128 34桁
やや物足りない。このライブラリは、メモリが許す限り、いくらでも長い精度を持つことができる。

 - C++ ヘッダと plain C ヘッダがある。
 - テスト済み:
     + 四則演算, Valgrind でメモリリークがない.
 - 要テスト
     + 数学関数
     + rounding mode
     + ±INF, NaN が絡むケース



### 数学関数

角度
  lf_angle(x, y)  点(x, y) の x 軸からの偏角, -pi < angle(x, y) <= pi
  lf_deg(x)       ラジアンを度に変換
  lf_rad(x)       度をラジアンに変換.
                                                                     IEEE 754
三角関数
  lf_arccos  逆余弦 arc cosine. cos<sup>-1</sup>(x). C では `acos()`     ✓
  lf_arcsin  逆正弦 arc sine. sin<sup>-1</sup>(x). C では `asin()`       ✓
  lf_arctan  逆正接 arc tangent. tan<sup>-1</sup>(x).                    ✓
  lf_cos    余弦 cosine                                                  ✓
  lf_cot    余接 cotangent   cot(x) = 1/tan(x)
  lf_cosec  余割 cosecant    cosec(x) = 1/sin(x)
  lf_sec    正割 secant      sec(x) = 1/cos(x)
  lf_sin    正弦 sine
  lf_tan    正接 tangent
  
対数関数, 指数関数, 双曲線関数  
  lf_cosh   双曲線余弦 hyperbolic cosine.   = (exp(x) + exp(-x)) / 2
  lf_exp(x) 指数関数 = e<sup>x</sup>
  log(x) ●未了  自然対数 = log<sub>e</sub>x = ln(x)
  log10(x) ●未了 常用対数 = log<sub>10</sub>x
  log2(x)  ●未了 2を底とする対数 = log<sub>2</sub>x
  lf_sinh  双曲線正弦 hyperbolic sine.  = (exp(x) - exp(-x)) / 2
  lf_tanh  双曲線正接 hyperbolic tangent.  = sinh(x) / cosh(x)

  lf_pi

IEEE 754 で推奨される関数: https://en.wikipedia.org/wiki/IEEE_754






## 今後の方向性

昔は BASIC っぽくしようとしていたようだが、電卓なので、プログラミング言語の真似をしてもなぁ、とも思う。

二つの方角がある。変数を宣言して変数に値を代入したり、計算できるようにする方向。大きな数値表を読み込んだり。もう一つが, 代数計算できるようにする方向。

これと別に, グラフを描けるようにするのも面白い。





## 先行事例

例えば, GNU Octave は、数値解析のためのプログラミング言語で、2D/3D plot もできる。MATLAB と互換性がある。シンボリックな計算はできない。<a href="https://www.scilab.org/software/scilab/">Scilab</a> も同様. 扱う対象は数値行列.

やや軽めのもので, gnome-genius パッケージも, 3Dグラフを表示できる。MPFR ベース。`function f(x) =` で関数定義。数値計算。

もっと簡単には, POSIX <kbd>bc</kbd>(1) コマンドぐらいでもいい。`if`, `for`, `while` といった制御構造, `define` でユーザ定義関数を作れる。


### 数式処理

<i>Maxima</i> は数式処理プログラム。式を式のまま計算していく。

もう少し軽いものだと, <i>Qalculate!</i> という電卓もある。MPFR ベース。式を変形してくれる。グラフも描ける。式の展開ができる (symbolic calculation)。
Fedora 37 パッケージあり。ただ、gtk版 (qalculate-gtk パッケージ) は大丈夫だったが、qt版 (qalculate-qt パッケージ) はメニューが正常に表示されなかった。手許の環境の問題かは不明。
KDE Cantor などいくつかのアプリケーションが Qalculate! を内部で利用。

ライブラリでは, Python 用の <i>SymPy</i> がメジャーのようだ。





## 応用

<a href="https://github.com/nakatamaho/mplapack/">nakatamaho/mplapack: The MPLAPACK: multiple precision version of BLAS and LAPACK</a>
バックエンド MPFR + MPC

<a href="http://midarekazu.g2.xrea.com/multi.html">多倍長整数演算, 世界最速の円周率の計算法 (円周率を4種類の方法で 10億桁求めました)</a>
10進BASIC のプログラムも。



