
#include <stdio.h>
#include <math.h>  // INFINITY, NAN
#include <errno.h>

int main()
{
    double x, y;
    x =   123.4567890123456789;   // 1.234567890123456806e+02
    y = 0.01234567890123456789;   // 1.234567890123456843e-02

    // 変換指定子 `g` は、`f` 形式または `e` 形式に変換する。デフォルト6桁.
    // `f`  [-]ddd.ddd 形式
    // `e`  [-]d.ddde±dd 形式.
    printf("x = %.16e, y = %.16e\n", x, y);

    printf("0 = %.16e\n", 0.0); //=> 0 = 0.0000000000000000e+00

    double n1 = +INFINITY;
    double n2 = -INFINITY;
    double n3 = NAN;
    printf("n1 = %.16e, n2 = %.16e, n3 = %.16e\n", n1, n2, n3);
    //=> n1 = inf, n2 = -inf, n3 = nan

    // "%+02d": '±' と '0' 埋めが両立しない。
    printf("%02d, %02d, %02d\n", 1, 100, -5);

    errno = 0;
    x = strtod(nullptr, NULL); // Segmentation fault. 第一引数に NULL は不可!
    printf("errno = %d\n", errno);

    return 0;
}
