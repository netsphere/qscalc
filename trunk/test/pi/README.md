# 円周率

## How to run

1億桁
<pre>
$ <kbd>./gmp-chudnovsky 100000000 1 > pi</kbd>
</pre>

ビビるぐらい速い!
環境
  CPU 4.0〜4.1 GHz
  Windows 10
  VMware Workstation 17
  Fedora Linux 37

<table>
  <tr><th>桁数  <th>時間
  <tr>
    <td>1,000
    <td>
#terms=70, depth=8
sieve   time =  0.000
...................................................

bs      time =  0.000
   gcd  time =  0.000
div     time =  0.000
sqrt    time =  0.000
mul     time =  0.000
total   time =  0.000
   P size=1334 digits (1.334000)
   Q size=1327 digits (1.327000)

  <tr>
    <td>1,000,000
    <td>
#terms=70513, depth=18
sieve   time =  0.002
..................................................

bs      time =  0.199
   gcd  time =  0.000
div     time =  0.029
sqrt    time =  0.015
mul     time =  0.012
total   time =  0.257
   P size=1455608 digits (1.455608)
   Q size=1455601 digits (1.455601)

  <tr>
    <td>100,000,000 (1億桁!)
    <td>
#terms=7051366, depth=24
sieve   time =  0.514
..................................................

bs      time = 59.145
   gcd  time =  0.000
div     time =  5.599
sqrt    time =  2.975
mul     time =  2.105
total   time = 70.369
   P size=145605885 digits (1.456059)
   Q size=145605879 digits (1.456059)

real    1m12.562s
user    1m10.371s
sys     0m1.604s

  <tr>
    <td>200,000,000
    <td>
#terms=14102733, depth=25
sieve   time =  1.126
..................................................

bs      time = 135.195
   gcd  time =  0.000
div     time = 12.193
sqrt    time =  6.492
mul     time =  4.657
total   time = 159.720
   P size=291210007 digits (1.456050)
   Q size=291210000 digits (1.456050)

real    2m50.398s
user    2m39.721s
sys     0m4.624s
</table>



