
#include "libqslargedecimal/lfloat.h"

// TODO: 有効桁数が, 後ろの"0"を考慮できていない?
// TODO: <s>3桁区切りは"'" よりも"_"のほうがメジャー. "'" はBASIC では行コメント.</s> すみ

int main()
{
  lfloat x("1234567890.1234567890000000000");
  lfloat y("98765400000000.95147258632100000000000");

  char buf[1000];
  printf("x = %s\n", x.get_str().c_str() );
  printf("y = %s\n", y.get_str().c_str() );

  return 0;
}
