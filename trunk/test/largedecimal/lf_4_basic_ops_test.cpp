
// 信頼できるものと同じ結果になることを確認する。
// このテストを回すときは基数 = 100 にすること。

#include <time.h>
//#include <gmpxx.h>
#include <decimal.hh>
#include "../../libqslargedecimal/lfloat.h"
#include "../../libqslargedecimal/large_priv.h"
#include <assert.h>
#include <string.h>
#include <math.h>
using namespace std;
using namespace decimal;

/*
文字列への変換
mpdecimal++ <decimal.hh> ヘッダ:
   `repr()` メソッド: Python 用
      Decimal("1.234E+100").repr()  => "Decimal(\"1.234E+100\")"
   `to_eng()` が通常の形. Plain C 版の `mpd_to_eng()` のラッパ.
   `format()` が文字列への変換のための表示形式を指定.
gmpxx.h
   `get_str()`  Plain C 版の `mpz_get_str()` のラッパ.
*/

void print_time()
{
    time_t t = time(NULL);
    struct tm local;
#ifdef _WIN32
    localtime_s(&local, &t);
#else
    localtime_r(&t, &local);
#endif

    char buf[100];
    // strftime() に渡すのは struct tm 型.
    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S %Z", &local); // %T POSIX
    printf("%s\n", buf);
}


void dump_qli(const char* s, const qlf_t* r)
{
    printf("%s m_size = %d, alloc_size = %u, frac = ",
           s, r->m_size, r->alloc_size);
    for (unsigned i = 0; i < r->alloc_size; ++i)
        printf(CFMT " ", r->frac[i]);
    printf("\n");
}

#if 0
template <typename LOpType, typename IOpType>
void do_test_dbl(bool do_when_zero = true)
{
    char ds[100];

    for (double i = -10000; i <= +10000; fabs(i) > 50 ? i += 7 : i += 1) {
        for (double j = -10000; j <= +10000; fabs(j) > 50 ? j += 7 : j += 1) {
            lfloat lu(i), lv(j);

            if (!do_when_zero && lv == 0)
                continue;

            lfloat lr = LOpType()(lu, lv); string ls = lr.get_str(15);
            double dr = IOpType()(i, j);
            if (dr == 0.0)
                strcpy(ds, "0");
            else {
                // %n.me  n は文字数. m は小数点以下の精度
                // "%.me" と書けば, 精度は m + 1
                sprintf(ds, "%.14e", dr); //2.3568396226e+01
            }

/*
  double の文字列変換は、10進で見ると安定していない。
        decimal                   double
 -  -9.37764932562620e+00    -9.37764932562621e+00
    frac = 09 37 76 49 32 56 26 20 42 38 92
                                   ^ これを切り上げている。
 -   7.10510948905109e+01     7.10510948905110e+01
    frac = 71 05 10 94 89 05 10 94 89 05
                                 ^ ここを切り上げ.
 -   4.26929824561404e+01     4.26929824561403e+01
    frac = 42 69 29 82 45 61 40 35 08 77 19 29 82
                                 ^ ここを切り捨て。上位桁が3 で奇数なので,
                                   最近接偶数への丸めでも切り上げたい.
 */
            if ( ls != ds ) {
                printf("Failed: i = %f, j = %f, ls = %s, s = %s\n",
                       i, j, ls.c_str(), ds );
                dump_qli("lu =", lu.get_cval());
                dump_qli("lv =", lv.get_cval());
                dump_qli("lr =", lr.get_cval());
                //abort();
            }
        }
    }
}
#endif


template <typename LOpType, typename IOpType>
void do_test(bool do_when_zero = true)
{
    string ds;

    for (Decimal i = -10000; i <= +10000; i.abs() > 50 ? i += 7 : i += 1) {
        for (Decimal j = -10000; j <= +10000; j.abs() > 50 ? j += 7 : j += 1) {
            lfloat lu(i.to_eng() ), lv(j.to_eng() );

            if (!do_when_zero && lv == 0)
                continue;

            lfloat lr = LOpType()(lu, lv); string ls = lr.get_str(16);
            Decimal dr = IOpType()(i, j);
            //            if (dr == Decimal("0.0") )
            //                ds = "0"; // strcpy(s, "0");
            //            else {
                // %n.me  n は文字数. m は小数点以下の精度
                // "%.me" と書いて, 精度は m + 1
            ds = dr.to_sci(); // sprintf(s, "%.10e", dr); 2.3568396226e+01
            //            }

            if ( atof(ls.c_str()) != atof(ds.c_str()) ) {
                printf("Failed: i = %s, j = %s, ls = %s, s = %s\n",
                       i.to_eng().c_str(), j.to_eng().c_str(),
                       ls.c_str(), ds.c_str() );
                dump_qli("lu =", lu.get_cval());
                dump_qli("lv =", lv.get_cval());
                dump_qli("lr =", lr.get_cval());
                //abort();
            }
        }
    }
}

int main()
{
    // lfloat
/*
    printf("add: "); print_time();
    do_test< std::plus<lfloat>, std::plus<Decimal> >();
    printf("sub: "); print_time();
    do_test< std::minus<lfloat>, std::minus<Decimal> >();
    printf("mul: "); print_time();
    do_test< std::multiplies<lfloat>, std::multiplies<Decimal> >();
*/
    printf("div: "); print_time();
    do_test< std::divides<lfloat>, std::divides<Decimal> >(false);

    print_time();

    return 0;
}
