
// 信頼できるものと同じ結果になることを確認する。
// このテストを回すときは基数 = 100 にすること。

#include <time.h>
#include <gmpxx.h>
#include "../../libqslargedecimal/largeint.h"
#include "../../libqslargedecimal/large_priv.h"
#include <assert.h>
using namespace std;

/*
文字列への変換
mpdecimal++ (decimal.hh)
   `repr()` メソッド: Python 用
      Decimal("1.234E+100").repr()  => "Decimal(\"1.234E+100\")"
   `to_eng()` が通常の形. Plain C 版の `mpd_to_eng()` のラッパ.
   `format()` が文字列への変換のための表示形式を指定.
gmpxx.h
   `get_str()`  Plain C 版の `mpz_get_str()` のラッパ.
*/

void print_time()
{
    time_t t = time(NULL);
    struct tm local;
#ifdef _WIN32
    localtime_s(&local, &t);
#else
    localtime_r(&t, &local);  // POSIX
#endif

    char buf[100];
    // strftime() に渡すのは struct tm 型.
    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S %Z", &local); // %T POSIX
    printf("%s\n", buf);
}


void dump_qli(const char* s, const qli_t* r)
{
    printf("%s m_size = %d, alloc_size = %u, frac = ",
           s, r->m_size, r->alloc_size);
    for (unsigned i = 0; i < r->alloc_size; ++i)
        printf(CFMT " ", r->frac[i]);
    printf("\n");
}


template <typename LOpType, typename IOpType>
void do_test(bool do_when_zero = true)
{
    char s[100];

    for (long i = -100'068'480; i <= +100'000'000; abs(i) > 110 ? i += 70027 : ++i) {
        for (long j = -100'068'480; j <= +100'000'000; abs(j) > 110 ? j += 70027 : ++j) {
            LargeInt lu(i), lv(j);

            if (!do_when_zero && lv == 0)
                continue;

            LargeInt lr = LOpType()(lu, lv);

            string ls = lr.get_str();
            sprintf(s, "%lld", IOpType()(i, j));
            if (ls != s) {
                printf("Failed: i = %ld, j = %ld, ls = %s, built-in = %s\n",
                       i, j, ls.c_str(), s);
                dump_qli("lu =", lu.get_cval());
                dump_qli("lv =", lv.get_cval());
                dump_qli("lr =", lr.get_cval());
                abort();
            }
        }
    }
}

int main()
{
    // GNU MP
/*
    printf("add: "); print_time();
    do_test< mpz_class, std::plus<mpz_class>, std::plus<int> >();
    printf("sub: "); print_time();
    do_test< mpz_class, std::minus<mpz_class>, std::minus<int> >();
    printf("mul: "); print_time();
    do_test< mpz_class, std::multiplies<mpz_class>, std::multiplies<int> >();
    printf("div: "); print_time();
    do_test< mpz_class, std::divides<mpz_class>, std::divides<int> >(false);
    print_time();
*/

    // QsLargeInt

    printf("add: "); print_time();
    do_test< std::plus<LargeInt>, std::plus<long long> >();
    printf("sub: "); print_time();
    do_test< std::minus<LargeInt>, std::minus<long long> >();
    printf("mul: "); print_time();
    do_test< std::multiplies<LargeInt>, std::multiplies<long long> >();
    printf("div: "); print_time();
    do_test< std::divides<LargeInt>, std::divides<long long> >(false);
    print_time();

    return 0;
}
