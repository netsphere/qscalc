
// C++ で INF, NaN になるケースを確認。

// $ gcc -Wall -Wextra cpp-error.cpp -lstdc++ -lm

/*
無限大については、この解説に近い。が、0^0 = 1.0 になったりする点が異なる。
    拡大実数系と不定形 | 実数の定義 | 実数 | 数学 | ワイズ
    https://wiis.info/math/real-number/definition-of-real-number/extended-real-number-system/

0^0 は定義できない。
例えば https://ja.wikipedia.org/wiki/0%E3%81%AE0%E4%B9%97

1/0 -> inf も不味い。無限小ε(イプシロン) を用意して, 1/ε→ ∞ ならまだよい。
この立場だと, 1/∞ → εにしないといけない (0ではない)。
例えば,
    超準解析入門 －超実数と無限大の数学－
    https://www.kurims.kyoto-u.ac.jp/~kenkyubu/kokai-koza/H29-isono.pdf
 */

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <exception>
using namespace std;

int main()
{
/* これでは捕捉できない。シグナル SIGFPE が発生.
    try {
        int ii = 1 / 0;
    } catch (exception& e) {
        cout << e.what() << "\n";
    }
 */

    // 0除算
    double a = +1.0 / +0.0;
    double b = +1.0 / -0.0;
    double nan = 0.0 / 0.0;
    double mnan = -0.0 / +0.0;  // マイナスNaN はない.
    printf("1/0 = %lf, 1/-0 = %lf, 0/0 = %lf, -0/0 = %lf\n", a, b, nan, mnan);
        //=> 1/0 = inf, 1/-0 = -inf, 0/0 = -nan, -0/0 = -nan

    // 無限大
    double inf = +INFINITY;
    double minf = -INFINITY;
    printf("∞ = %lf, -∞ = %lf\n", inf, minf);
    printf("∞ + 1 = %lf, ∞ + ∞ = %lf, ∞ + NaN = %lf\n",
           inf + 1, inf + inf, inf + nan);
    printf("∞ - 1 = %lf, ∞ - ∞ = %lf, ∞ - NaN = %lf\n",
           inf - 1, inf - inf, inf - nan);
    printf("∞ * 0 = %lf, ∞ * ∞ = %lf\n", inf * 0.0, inf * inf);
    printf("1/∞ = %lf, 0/∞ = %lf, ∞/1 = %lf, ∞ / ∞ = %lf, ∞ / 0 = %lf\n",
           1.0 / inf, 0.0 / inf, inf / 1.0, inf / inf, inf / 0.0);

    printf("∞ = -∞ -> %d, ∞ = |-∞| -> %d\n", inf == minf, inf == fabs(minf));
    printf("0^0 = %lf, ∞^0 = %lf, (-∞)^0 = %lf, 1^∞ = %lf, 1^-∞ = %lf\n",
           pow(0, 0), pow(inf, 0), pow(minf, 0), pow(1, inf), pow(1, minf));
    //=> 0^0 = 1.000000, ∞^0 = 1.000000, (-∞)^0 = 1.000000, 1^∞ = 1.000000, 1^-∞ = 1.000000

    // イプシロン. 0 と別に定義される。
    printf("epsilon = %.20le\n", DBL_EPSILON);

    // オーバフロー
    double d = exp(750);
    printf("exp^750 = %lf\n", d);

    return 0;
}
