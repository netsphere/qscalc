
/*
任意精度の10進浮動小数点演算ライブラリ

mpdecimal-devel パッケージ. Python3 が利用 (Fedora パッケージあり).
  Document .. https://www.bytereef.org/mpdecimal/

  For plain C  /usr/include/mpdecimal.h
  For C++      /usr/include/decimal.hh  内部で mpdecimal.h を include.

  $ gcc -Wall -Wextra decimal128.cpp -lstdc++ -lmpdec++ -lmpdec
*/

#include <stdio.h>
#include <decimal.hh>
#include <iostream>

mpd_context_t my_ctx;

int main()
{
  // C++ 版

  printf("sizeof Decimal = %ld, default prec = %ld\n",
         sizeof(decimal::Decimal), decimal::context.prec());
  //=> sizeof Decimal = 80, default prec = 16
  decimal::context.prec(50);
  decimal::Decimal d1("1.0"); decimal::Decimal d3("3.0");
  decimal::Decimal x = d1 / d3;

  std::cout << d1 << "/" << d3 << " -> " << x << "\n";
  //=> 1.0/3.0 -> 0.33333333333333333333333333333333333333333333333333

  //////////////////////////////////
  // Plain C では mpd_t 型を使う.

  // prec = 34, emax = 6144, emin = -6143
  mpd_ieee_context(&my_ctx, MPD_DECIMAL128);

  mpd_t* m1 = mpd_new(&my_ctx); mpd_set_string(m1, "5.0", &my_ctx);
  mpd_t* m3 = mpd_new(&my_ctx); mpd_set_string(m3, "3.0", &my_ctx);
  // status を返す mpd_qdiv() もある.
  mpd_t* xx = mpd_new(&my_ctx); mpd_div(xx, m1, m3, &my_ctx);

  printf("sizeof mpd_t = %ld, prec = %ld\n", sizeof(mpd_t), my_ctx.prec);
  //=> sizeof mpd_t = 48, prec = 34

  // mpd_print() は改行を付ける.
  mpd_print(m1); printf("/"); mpd_print(m3); printf(" -> ");
  mpd_print(xx);
  //=>   -> 1.666666666666666666666666666666667

  // 文字列への変換
  char* s = NULL;
  int len = mpd_to_eng_size(&s, xx, 0);
  printf("%s\n", s); //=> 1.666666666666666666666666666666667
  mpd_free(s); s = NULL;

  // 整数に丸める
  mpd_t* res = mpd_new(&my_ctx);
  mpd_round_to_int(res, xx, &my_ctx);
  len = mpd_to_eng_size(&s, res, 0);
  printf("%s\n", s); //=> 2
  mpd_free(s); s = NULL;
  mpd_del(res);

  mpd_del(m1); mpd_del(m3); mpd_del(xx);

  return 0;
}
