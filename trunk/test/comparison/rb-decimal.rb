
# Ruby は、有効精度がいい加減。これはアカン。
# ここがいい加減だと、わざわざ10進を使う意味が乏しい。

require 'bigdecimal'
require 'bigdecimal/util'

puts BigDecimal.limit(10)
a = BigDecimal('1.23456789449', 8)
p a, a.precs  #=> 0.123456789449e1, [27, 27]

puts BigDecimal.limit(11)
a = BigDecimal('1.23456789012345678901234567890123456789012345', 40)
  #=> 0.123456789012345678901234567890123456789012345e1, [54, 54]
p a, a.precs


num = 98.00000000000001.to_d
p num, num.class
# BigDecimal では丸めモードが7つある。ROUND_UP, HALF_DOWN は無用.
p BigDecimal::ROUND_UP # 1   無用. CEILING を使え.
# BigDecimal::ROUND_DOWN  # 2
# BigDecimal::ROUND_FLOOR  # 6
# BigDecimal::ROUND_HALF_UP          四捨五入. BigDecimal デフォルト.
# BigDecimal::ROUND_HALF_EVEN # 7    四捨6入. IEEE 754 の推奨.
p num.round(0, 1)  #=> 0.99e2   OK

num = 0.009
p num, num.class   #=> 0.009, Float
p num.to_d, num.to_d.class  #=> 0.9e-2, BigDecimal

p 0.07.to_d  #=> 0.7e-1
