
// gcc には __float128 型がある。C標準では _Float128. 同じか?
// -> __float128 と _Float128 は同じ型。後者のほうがポータブル。こちらを使え.

/*
C言語の long double は, 処理系依存。
 - gcc はコンパイルオプション:
     -mlong-double-64   long double は double 型と同じ。32bit Bionic C library のデフォルト.
     -mlong-double-80   Linux/glibc のデフォルト.
     -mlong-double-128  long double は __float128 型と同じ。64bit Bionic C library のデフォルト。
   ABI が壊れるので、指定すべきではない。デフォルトは OS / 環境 (libc) による。

 - MSVC は long double は, 64bit 環境であっても, double と同じ.

 - Power LE 上の AIX では, long double は double-double 型. Fedora は IEEE binary128 への移行を計画. https://fedoraproject.org/wiki/Changes/PPC64LE_Float128_Transition

<quadmath.h> ヘッダで多くの関数が定義. libquadmath-devel パッケージ
__float128 が前提.
*/

#define __STDC_WANT_IEC_60559_TYPES_EXT__ 1
#include <stdio.h>
#include <stdlib.h> // strfromf128()

int main()
{
  __float128 f1;
  _Float128 f2;  // ‘_Float128’ {aka ‘__float128’}. 同じ型

  printf("sizeof __float128 = %ld, _Float128 = %ld\n", sizeof(f1), sizeof(f2) );
  f1 = f2;   // 相互に代入OK.
  f2 = f1;

  //f2 = 1.0f128 / 3.0; unable to find numeric literal operator ‘operator""f128’
  // まだ未対応. See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=87274
  f2 = 1.0q / 3.0;

  char buf[1000];
  strfromf128(buf, sizeof(buf), "%a", f2);
  printf("1/3 -> %s\n", buf);

  return 0;
}
