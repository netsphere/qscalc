
// _Decimal128 が使えるか

/*
1. C++: /usr/include/c++/12/decimal/decimal ファイルを include する.

2. ライブラリをインストールする. libdfp-devel
  -> printf() 周りのコンパイルはできるようになったが、printf の表示が短い。
     まだアカンか?
*/

#define __STDC_WANT_DEC_FP__ 1
#define __STDC_WANT_IEC_60559_TYPES_EXT__ 1
#define __STDC_WANT_IEC_60559_DFP_EXT__ 1
// Plain C (since C23)  _Decimal32, _Decimal64, _Decimal128 型など
#include <stdio.h>
#include <stdlib.h>

// ISO/IEC TR 24733:2011 - C++03の拡張. std::decimal::decimal128 型など
#include <decimal/decimal>

// libdfp - Decimal Floating Point C Library
//#include <dfp/decimal/decimal>

// decimal128 型:
//     保持する値は __decfloat128 型.
//     typedef float __decfloat128 __attribute__((mode(TD)));
//       -> DECIMAL_FLOAT_MODE (TD, 16, decimal_quad_format);

// 表示する部分が gcc ではインストールされない。無理やり使う方法::
// https://stackoverflow.com/questions/12865585/stddecimaldecimal64-correct-usage-g-4-6-3

#include <iostream>
using namespace std;
using namespace std::decimal;

// やむなく, float に変換してから表示する。
// binary128 は仮数部112ビット (実質113ビット), 10進数で33桁程度
// decimal128 は基数=10進数, 34桁. ほぼ同じ.
ostream& operator << (ostream& out, const decimal128& value)
{
  _Float128 f = ((decimal128&) value).__getval();
  char buf[1000];
  strfromf128(buf, sizeof(buf), "%.40g", f);
  return out << buf;
}

int main()
{
  //_Decimal32 dec1;  まだ gcc に存在しない. コンパイルエラー
  //_Decimal64 dec2;
  //_Decimal128 dec3;

  decimal32 d1 = decimal32(1.0) / decimal32(3.0);
  decimal128 x = decimal32(1.0) / decimal64(3.0);
  decimal128 y = decimal128(1.0) / decimal128(3.0);
  
  //printf("dec1 = %f, dec2 = %f\n", d1, x);
  cout << "x = " << x << ", y = " << y << "\n";
  //=> x = 0.333333333333333300000000000000000022665,   有効桁数が短い!
  //=> y = 0.3333333333333333333333333333333333172839
  
  return 0;
}
