# 算術ライブラリ

さまざまなライブラリがある。GNU MPFR がメジャーで文書も充実しているが、基数 radix が 2.


### <a href="https://www.davidhbailey.com/dhbsoftware/">High-Precision Software Directory</a>

任意精度パッケージ <i>MPFUN2020</i> (旧 MPFUN2015); double-quad (8倍精度) <i>DQFUN</i> (旧QD) -- 約66桁; double-double (4倍精度) <i>DDFUN</i>. 多くの論文で QD と書かれているもの。

DD/QD は任意精度よりも速度で有利。DD は `_Float128` 型が標準化されたので、役割を終えるか。QD は66桁もあれば、かなりの領域をカバーできる。`double` 型が 8バイト. AVX2 (256bit) で, 4つの `double` 演算を同時に実行できる。ヘッダ: `/usr/lib/gcc/x86_64-redhat-linux/12/include/immintrin.h` .. gcc, clang が提供.



### <a href="https://www.mpfr.org/">The GNU MPFR Library</a>

<em>正しい丸め</em>による多倍長浮動小数点計算. De facto スタンダード。基数は 2. 精度は, コンテキストではなく、それぞれの値が持つ。

```c
/* Definition of the main structure */
typedef struct {
  mpfr_prec_t  _mpfr_prec;
  mpfr_sign_t  _mpfr_sign;
  mpfr_exp_t   _mpfr_exp;
  mp_limb_t   *_mpfr_d;
} __mpfr_struct;

typedef __mpfr_struct mpfr_t[1];
typedef __mpfr_struct *mpfr_ptr;
```


GMP (GNU MP) を使う場合であっても, 浮動小数点数については MPFR のインタフェイスを使うこと (GMP のドキュメントでも推奨).
`<mpf2mpfr.h>` ヘッダが `mpf_t` 型から `mpfr_t` 型への読み替えを提供しているが、挙動が違うので、きちんと書き直したほうがよい。

GNU MP の浮動小数点数 `mpf_t` は, 構造体のつくり上, INF, NaN, 負の 0 を持てない。
```c
typedef struct
{
  int _mp_prec;	/* Max precision, in number of `mp_limb_t's.
			   Set by mpf_init and modified by
			   mpf_set_prec.  The area pointed to by the
			   _mp_d field contains `prec' + 1 limbs.  */
  int _mp_size;	/* abs(_mp_size) is the number of limbs the
			   last field points to.  If _mp_size is
			   negative this is a negative number.  */
  mp_exp_t _mp_exp;	/* Exponent, in the base of `mp_limb_t'.  */
  mp_limb_t *_mp_d;	/* Pointer to the limbs.  */
} __mpf_struct;

/* typedef __mpf_struct MP_FLOAT; */
typedef __mpf_struct mpf_t[1];
```






### mpdecimal: C++ `<decimal.hh>`, plain C `<mpdecimal.h>`

Python で利用されている。

精度はコンテキスト `Context` クラスが保持する。

Plain C 用の `mpd_t` 型と, その C++ ラッパの `Decimal` 型. 64bit環境では, 内部は 10<sup>19</sup> 進数.

```c
/* mpd_t */
typedef struct mpd_t {
    uint8_t flags;       // MPD_SPECIAL (∞, NaN, signaling NaN) など
    mpd_ssize_t exp;
    mpd_ssize_t digits;
    mpd_ssize_t len;
    mpd_ssize_t alloc;
    mpd_uint_t *data;    // 64bit環境では uint64_t
} mpd_t;
```



### kv - C++による精度保証付き数値計算ライブラリ
http://verifiedby.me/kv/

4倍精度 (DD), MPFRラッパがある。



### <a href="http://www.oishi.info.waseda.ac.jp/~samukawa/">有理数計算プログラミング環境</a>

C++クラス. 多倍長整数 `longint` class, 有理数 `rational` class, 区間算術 `interval` class, 行列演算.



### <i>Boost.Multiprecision</i>

整数, 浮動小数点, 有理数, 複素数
 - 浮動小数点バックエンド `cpp_dec_float<N>` (10進), `mpfr_float<N>` (MPFRバックエンド), `float128`.
 - 複素数バックエンド `mpc` (GNU MPC: MPFR 上に構築された複素数ライブラリ)



### exflib
http://www-an.acs.i.kyoto-u.ac.jp/~fujiwara/exflib/

コンパイル時に精度を指定。


