# BASIC 処理系

探してみると, いくつも現役で開発が継続している。Fedora 37 には, gambas3 だけパッケージがある。逆に mono-basic パッケージで VB.NET が実行できる。

openSUSE Leap 15.4 は mono-basic のみ。


### 十進BASIC (Decimal BASIC)

ISO/IEC 10279 Full BASIC に準拠した処理系。英語版もあり、Linux (Qt5), macOS 版もある。まずはこれが有力。



### BASIC-256
https://sourceforge.net/projects/kidbasic/

Debian 11 パッケージあり。

図形も描ける。`LET` 文がない. コメントが "`#`" か? (`REM` も使える.) 独自っぽい感じ。スプライト機能あり. `SpriteLoad` 文で画像をスプライトとして読み込む。




### BBC BASIC
https://www.bbcbasic.co.uk/bbcbasic.html
CP/M から Windows まで. UNIX 版もある。歴史的な感じ。

Debian 11: brandy パッケージ。




### Gambas
https://gambas.sourceforge.net/en/main.html

Debian 11 パッケージあり。Fedora 37 パッケージあり。これが次点か。

クラスがある。`As` キーワードで型を示す。VB みたい。




### Yabasic
<a href="http://2484.de/yabasic/">Yabasic, Yet another Basic for Unix and Windows</a>

Debian 11 パッケージあり。

`LET` 文はない。`WHILE` ～ `WEND` (VB6.0には同じキーワードがある), `IF` の条件式がカッコ必須, `ELSIF` (VB6.0, Full BASIC は `ELSEIF`. 1文字節約なぜ?) と、独自感がある。



### FreeBASIC
https://www.freebasic.net/





