﻿// -*- coding:utf-8-with-signature -*-

// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#include <ctype.h>
#include <assert.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <stdexcept>  // `runtime_error`
using namespace std;

#ifndef _WIN32
  #define _atoi64 atoll
#endif

#include "lfloat.h"
#include "large_priv.h"

////////////////////////////////////////////////////////////////////////////
// qlf_context

constexpr int div_ceil(int x, int y) {
    return (x + (y - 1)) / y;
}

// TODO: スレッドローカルにすること。
static qlf_context default_ctx = {
    .prec10 = 100,
    .max_prec = div_ceil(100, PACK), // ceil(prec10 / PACK)
    .round = QLF_ROUND_HALF_UP,
    .error_mode = 0,
};

/**
 * 精度を10進の桁数で指定.
 * @param new_prec  10進での桁数
 */
void qlf_set_prec(qlf_context* ctx, int new_prec)
{
    assert(new_prec > 0);

    if (!ctx)
        ctx = &default_ctx;

    ctx->prec10 = new_prec;
    // 切り上げる. ceil()
    ctx->max_prec = div_ceil(new_prec, PACK);
}


////////////////////////////////////////////////////////////////////////////
// 初期化・代入

static qlf_t* set_zero(qlf_t* r) {
    r->m_sign = 0;
    r->m_size = 0;
    r->exp = QLF_ZERO_EXP;
    return r;
}


/**
 * 初期化
 */
qlf_t* qlf_init(qlf_t* r)
{
    assert( r );

    set_zero(r);
    r->alloc_size = 0; r->frac = nullptr;
    return r;
}


// Copy ctor
lfloat::lfloat(const lfloat& a)
{
    qlf_init_copy(&val, &a.val);
}

lfloat::lfloat(const char* s)
{
    qlf_init_set_str(&val, s, NULL);
}

lfloat::lfloat(const string& str)
{
    qlf_init_set_str(&val, str.c_str(), NULL);
}


lfloat::lfloat(double a)
{
    char s[24];
    sprintf(s, "%.16e", a); // 手抜き. doubleだと，この辺が限界
    qlf_init_set_str(&val, s, NULL);
}


void qlf_clear(qlf_t* r) {
    set_zero(r);
    r->alloc_size = 0; free(r->frac); r->frac = nullptr;
}


lfloat::~lfloat()
{
    qlf_clear(&val);
}


qlf_t* qlf_init_copy(qlf_t* r, const qlf_t* u) {
    qlf_init(r);
    return qlf_copy(r, u);
}

qlf_t* qlf_init_set_str(qlf_t* r, const char* str, const char** endptr ) {
    qlf_init(r);
    return qlf_set_str(r, str, endptr);
}


// 変換 //////////////////////////////////////////////////////////////////

double qlf_get_dbl(const qlf_t* v)
{
    assert( v );

    if ( qlf_isinf(v) )
        return v->m_sign ? -INFINITY : INFINITY;
    else if ( qlf_isnan(v) )
        return NAN;

    const int alloc = v->m_size * PACK + 20;
    char* s = new char[alloc];  // '20'に意味はない。
    double r = atof(qlf_get_str(v, s, alloc, 0));
    delete [] s;

    return r;
}


/**
 * lfloat -> str
 *   1.23450000e+02   先頭の1桁 '.' (n_digits - 1桁) 'e±nn'
 * @param n_digits 表示する桁数. 0 の場合は, 保持するまま表示.
 */
char* qlf_get_str(const qlf_t* val, char* str, int size, int n_digits )
{
    assert( val && str && size > 0 );

    if ( qlf_iszero(val) )
        return strcpy(str, "0"); // これはまずい. TODO: "0.00000e+00"
    if ( qlf_isinf(val) )
        return strcpy(str, val->m_sign ? "-INF" : "INF");
    else if ( qlf_isnan(val) )
        return strcpy(str, "NAN");

    const qlf_context* ctx = &default_ctx;

    // 一時的な小数部の保存領域。
    const int D = max(val->m_size, (unsigned) div_ceil(n_digits, PACK));
    // 1. 切り上がって桁が上がる. 2. 10進での開始位置が (PACK - 1) 桁ずれる.
    // 3. sprintf() の '\0'
    char* acc = (char*) calloc( D + 3, PACK);

    // 1. 小数部を10進に変換する
    for (int ii = 0; ii < D + 1; ++ii)
        sprintf(acc + ii * PACK + 1, CFMT, ii < val->m_size ? val->frac[ii] : 0);

    // 2. 10進での指数を調べ，小数部の先行する0を除く
    int e10 = val->exp * PACK;
    char* fst = acc + 1;
    while (*fst == '0') {
        fst++; e10--;
    }

    if (n_digits) {
        // 表示精度を指定する場合, 切り捨てる.
        // round()とほとんど同じ。ただし'0'〜'9'を使う
        int k = fst[n_digits] >= '5'; //||
                 // (fst[n_digits] == '5' && (fst[n_digits - 1] - '0') % 2);
        for (int j = n_digits - 1; j >= 0 && k; --j) {
            if (fst[j] == '9') {
                fst[j] = '0'; k = 1;
            } else {
                fst[j]++; k = 0;
            }
        }
        if (k) {
            *--fst = '1'; e10++;
        }
    }
    else {
        // 末尾の余計な '0' を除く
        n_digits = val->m_size * PACK - (fst - (acc + 1));
        while (fst[n_digits - 1] == '0') --n_digits;
    }

    // 5. 出力
    char* p = str;    // str は戻り値に使う
    if ( val->m_sign )
        *(p++) = '-';
/*
    if (e10 >= -3 && e10 <= +14) {
        // 123.45形式
        if (e10 > 0) {
            // 絶対値1以上
            for (i = 0; i < flen; i++) {
                if (i == e10)
                    *p++ = '.';
                *p++ = fst[i];
//                if (!((e10 - i - 1) % 3) && i < e10 - 1)
//                    *p++ = '_';
            }
            for (i = 0; i < (e10 - flen); i++) {
                *p++ = '0';
//                if (!((i - e10 + flen + 1) % 3) && i < e10 - flen - 1)
//                    *p++ = '_';
            }
        }
        else {
            // 絶対値1未満
            *(p++) = '0'; *(p++) = '.';
            for (i = 0; i < -e10; i++)
                *(p++) = '0';
            strncpy(p, fst, flen);
            p += flen;
        }
        *p = '\0';
    }
    else {
*/
    // 1.2345e+nn形式
    *p++ = fst[0]; *p++ = '.';
    for (int i = 1; i < n_digits; ++i)
        *p++ = fst[i];
    *p++ = 'e';
    *p++ = e10 > 0 ? '+' : '-'; // "%+02d" では 0 で埋まらない.
    sprintf(p, "%02d", abs(e10 - 1) );

    free(acc);
    return str;
}


// @param n_digits 出力する桁数. 0 なら全部.
string lfloat::get_str( int n_digits ) const
{
    if (qlf_isnan(&val))
        return "NAN";
    if (qlf_iszero(&val))
        return "0";  // +0, -0 は区別しない
    if (qlf_isinf(&val))
        return !val.m_sign ? "INF" : "-INF";

    // ちょっと多めに確保する
    // 符号, 小数点, 'e', 指数 3桁, '\0'. これで7桁.
    int siz = max(val.m_size * PACK, (unsigned) n_digits) + 10;  // -1.23456e+02
    char* s = (char*) calloc(siz, 1);
    qlf_get_str(&val, s, siz, n_digits );
    string ret(s); free(s);

    return ret;
}


////////////////////////////////////////////////////////////////////////////
// 初期化・代入

/**
 * r = u. 初期化を兼ねない!
 * @return r
 */
qlf_t* qlf_copy(qlf_t* r, const qlf_t* u)
{
    assert( r && u );
    if (r == u)
        return r;

    if ( qlf_iszero(u) ) {
        // 浅い初期化のみ
        set_zero(r);
        return r;
    }
    else if ( !qlf_isfinite(u) ) {
        if ( qlf_isinf(u) ) {
            r->m_sign = u->m_sign;
            r->m_size = 0; // delete [] r->frac; r->frac = nullptr;
        }
        else { // NaN
            r->m_sign = 0;
            r->m_size = (uint32_t) -1; // delete [] r->frac; r->frac = nullptr;
        }
        return r;
    }

    if ( u->m_size > r->alloc_size ) {
        r->alloc_size = u->m_size + 1;
        free(r->frac);
        r->frac = (ql_s_t*) calloc( r->alloc_size, sizeof(ql_s_t) );
    }
    r->m_sign = u->m_sign;
    r->m_size = u->m_size;
    r->exp = u->exp;
    memcpy(r->frac, u->frac, u->m_size * sizeof(ql_s_t) );

    return r;
}


/**
 * TODO: この関数は効率が悪い。val を丸めるのではなく、丸めた結果を val に設定
 *       するようになっている。メモリ確保が二度手間になっている。

 *  (max_prec + 1)桁目を四捨五入してmax_prec桁目まで求め, val に代入する。
 * @param u  長さ u_size
    //      u[]は，0が先行することもあるし，末尾が0になっていることもある。
 * @return val
 */
static qlf_t* round(qlf_t* val, unsigned signbit, int new_exp,
                    ql_s_t u[], int u_size)
{
    assert(val);
    assert( u && u_size > 0);

    const qlf_context* ctx = &default_ctx;

    int i, j, k = 0;

    // 先行する0を除く
    ql_s_t* fst = u;
    while (!*fst) {
        fst++; new_exp--;
    }

    // 四捨五入する。
    int flen = u_size - (fst - u);
    if (flen > ctx->max_prec) {
        k = fst[flen = ctx->max_prec] >= BASE / 2;
        for (j = ctx->max_prec - 1; j >= 0 && k; j--) {
            if (fst[j] == BASE - 1) {
                fst[j] = 0; k = 1; flen--;  // ここでflenを切り詰めておくと，次の小数部の長さを求めるところが速くなる。
            }
            else {
                fst[j]++; k = 0;
            }
        }
    }
    if (k) {
        fst[0] = 1; flen = 1; new_exp++;
    }

    // 小数部の長さを求める
    for (i = flen - 1; i >= 0; i--) {
        if (!fst[i])
            flen--;
        else
            break;
    }
    if (i < 0) {
        // 四捨されて，0になっている. 浅い初期化.
        set_zero(val);
        return val;
    }

    // 値を設定
    if (val->alloc_size < flen) {
        val->alloc_size = flen + 1;
        free(val->frac);
        val->frac = (ql_s_t*) calloc(val->alloc_size, sizeof(ql_s_t));
    }
    val->m_sign = signbit;
    val->m_size = flen;
    val->exp = new_exp;
    memcpy(val->frac, fst, flen * sizeof(ql_s_t));

    return val;
}


/**
 * @param s 次の構文
 *       ({ui_c}(\.)?|{ui_c}?\.{unsigned_integer})(E[-+]?{unsigned_integer})?
 *          数値列に "_" または "'" が含まれていてもよい.
 *          NULL の場合, errno に EINVAL を設定したうえで, r に 0 を設定する。
 * C99標準では, "[-]inf", "[-]infinity", `nan`. 大文字・小文字を区別しない。
 *
 * @return r
 *
 * Note. atof() は時代遅れ. strtod() を使え.
 */
qlf_t* qlf_set_str( qlf_t* r, const char* s, const char** endptr )
{
    assert(r);  // s = NULLもあり。

    // 浅い初期化
    set_zero(r);
    //delete [] r->frac; r->frac = nullptr;

    if (!s) {
        if (endptr) *endptr = s;
        errno = EINVAL;
        return r; // 値 = 0
    }

    const char* start = nullptr;  // 有意な小数部の先頭
    int e = 0;      // 10進での指数
    int si = 0;     // 符号. -1 でマイナス

    // 1. white-space はスキップする。
    while ( *s == ' ' || *s == '\t' ) s++;
    // 符号
    if (*s == '-') {
        si = -1; s++;
    }
    else {
        si = +1;
        if (*s == '+') s++;
    }

    // 2. Infinity or NaN
    if ( !strcasecmp(s, "inf") || !strcasecmp(s, "infinity") ) {
        r->m_sign = si > 0 ? 0 : 1;
        r->exp = QLF_INFINITE_EXP;
        r->m_size = 0;
        return r;
    }
    else if ( !strcasecmp(s, "nan") ) {
        r->m_sign = 0;
        r->exp = QLF_INFINITE_EXP;
        r->m_size = (uint32_t) -1;
    }

    // 同じ"12345"でも12, 3450のときもあれば1234, 5000となることもある。
    // 初めに BASE 進数での指数を求めてから，数字列の最初に戻って読む
    if (isdigit(*s)) {
        while ( *s == '0' || *s == '_' || *s == '\'' ) s++; // 先行する 0
        // 000123
        //    ^
        if (*s >= '1' && *s <= '9') {
            start = s;
            do {
                if (isdigit(*s)) e++;
            } while (s++, (isdigit(*s) || *s == '_' || *s == '\''));
            if (*s == '.') {
                s++; goto L1;
            }
            goto L2;
        }
    }

    if (*s == '.') {    // ".0000xx" の形
        while (*(++s) == '0')   // 小数点以下は "_" は不可. TODO: 可にする?
            e--;
        if (*s >= '1' && *s <= '9') {
            start = s; goto L1;
        }
    }

    // 0000.0000e
    //          ^
    if (endptr) *endptr = s;
    return r; // 値 0

L1:
    // 小数点以下
    while (*s && isdigit(*s))
        s++;
L2:
    // 指数
    if (*s == 'e' || *s == 'E') {
        int ei = e + atoi(s + 1);
        int64_t e64 = int64_t(e) + _atoi64(s + 1);
        if (e64 != ei)
            throw lf_exception(1001, "オーバーフロー");
        e = ei;
    }

    // BASE 進数に変換
    r->exp = e > 0 ? (e - 1) / PACK + 1 : e / PACK;

    // 小数部
    int flen = strlen(start) / PACK + 2;    // "1.2" -> 1, 2000
    ql_s_t* acc = (ql_s_t*) calloc(flen, sizeof(ql_s_t) );
    s = start;
    int n = (PACK - e % PACK) % PACK;
    for (ql_s_t* p = acc; ; n = 0, p++) {
                    // 指数部によって，小数点が桁の途中で出現することがある。
                    // "1.2"    -> 0001 2000
                    // "1.2e-1" -> 1200
        for (unsigned i = n; i < PACK; i++) {
redo:
            if (*s == '.' || *s == '_' || *s == '\'') {
                s++;
                goto redo;
            }
            if (isdigit(*s)) {
                *p = *p * 10 + (*s - '0');
                s++;
            }
            else if (!*s || *s == 'e' || *s == 'E') {
                for (unsigned j = i; j < PACK; j++)
                    *p *= 10;   // 数字は上から詰めていく
                goto label2;
            }
            else { // error
                free(acc);
                if (endptr) *endptr = s;
                return set_zero(r);
            }
        }
    }
label2:

    round(r, si >= 0 ? 0 : 1, r->exp, acc, flen);
    free(acc);

    return r;
}


////////////////////////////////////////////////////////////////////////////
// 算術演算

// r := u + v; u <- u + v でもよい.
// public
qlf_t* qlf_add( qlf_t* r, const qlf_t* u, const qlf_t* v )
{
    assert( r && u && v );

    if ( qlf_isnan(u) || qlf_isnan(v) )
        return qlf_set_str(r, "NAN", NULL);

    // どちらかが0の時
    if ( qlf_iszero(u) ) {
        if (qlf_iszero(v)) return set_zero(r);
        return round(r, v->m_sign, v->exp, v->frac, v->m_size);
    }
    if ( qlf_iszero(v) ) return round(r, u->m_sign, u->exp, u->frac, u->m_size);

    // u, vの符号が異なるときは減算ルーチンを呼ぶ
    if ( u->m_sign != v->m_sign ) {
        qlf_t vn; qlf_copy(qlf_init(&vn), v);
        qlf_sub( r, u, qlf_negate(&vn) );
        qlf_clear(&vn);
        return r;
    }

    const qlf_context* ctx = &default_ctx;

    // A2. eu >= evとする
    const qlf_t *up, *vp;  // eu >= evとするため
    if ( u->exp >= v->exp ) {
        up = u; vp = v; }
    else {
        up = v; vp = u; }

    // A4. 指数部の差が十分大きいときは大きい方を答えとする。
    //      0.9999                   0.9999
    //     +0.00009                 +0.000009
    //     --------                 ---------
    //      0.99999 -> 1.000         0.999909 -> 0.9999
    int ediff = up->exp - vp->exp;
    if (ediff >= ctx->max_prec + 1)
        return qlf_copy(r, up);

    // 累算器(accumulator)に設定
    //       0.9999
    //      +0.00009999
    //      -----------
    //       0.99999999 -> 1.000
    // acc1: * ********
    // acc2:   ********
    // 丸めモードを変更する場合があり、精度いっぱいまで計算してから丸める.
    const int acsize = max(up->m_size, vp->m_size + ediff);
    ql_s_t* acc1 = (ql_s_t*) calloc(acsize + 1, sizeof(ql_s_t));
    memcpy(acc1 + 1, up->frac, up->m_size * sizeof(ql_s_t));

    // A6. 加算する。小数点以下第1位から第(prec + 1)桁まで計算する。
    acc1[0] = n_add(acc1 + 1, up->m_size, vp->frac, vp->m_size, ediff);

    // r に書き戻す
    round(r, up->m_sign, up->exp + 1, acc1, acsize + 1);
    free(acc1); //free(acc2);

    return r;
}


// public
// r := u - v
// add()と似てるが，正規化の手順が異なる
qlf_t* qlf_sub( qlf_t* r, const qlf_t* u, const qlf_t* v )
{
    assert( r && u && v );

    const qlf_t *up, *vp;  // |u| >= |v|
    int si = 0;

    if ( qlf_isnan(u) || qlf_isnan(v) )
        return qlf_set_str(r, "NAN", NULL);

    // どちらかが0のとき
    if ( qlf_iszero(u) ) {
        qlf_copy(r, v); r->m_sign = 1 - r->m_sign;
        return r;
    }
    else if ( qlf_iszero(v) )
        return qlf_copy(r, u);

    // u, vの符号が異なるときは加算ルーチンを呼ぶ
    if ( u->m_sign != v->m_sign ) {
        qlf_t vn; qlf_copy(qlf_init(&vn), v);
        qlf_add(r, u, qlf_negate(&vn) );
        qlf_clear(&vn);
        return r;
    }

    const qlf_context* ctx = &default_ctx;

    // 符号は同じ. |u| >= |v|とする
    switch (qlf_compare(u, v))
    {
    case +1:    // u > v
        up = !u->m_sign ? u : v;   vp = !u->m_sign ? v : u;
        si = +1;
        break;
    case 0:     // u == v
        return set_zero(r);
    case -1:    // u < v;
        up = !u->m_sign ? v : u;   vp = !u->m_sign ? u : v;
        si = -1;
        break;
    default:
        abort();
    }

    // 指数の差が十分大きいときは大きい方を解とする
    //       0.1000                  0.1000
    //      -0.000009               -0.0000009
    //      ---------               ----------
    //       0.099991 -> 0.09999     0.0999991 -> 0.1000
    // TODO: 丸めモードによってはこれは不味い。末尾を 1だけ引くようにする。
    const int ediff = up->exp - vp->exp;
    if (ediff >= ctx->max_prec + 2)
        return qlf_copy(r, up);

    // 累算器(accumulator)に設定
    //       0.1000
    //      -0.000009999
    //      ------------
    //       0.099990001 -> 0.09999
    // acc1:   *********
    // acc2:   *********
    int acsize = max( up->m_size, vp->m_size + ediff);
    ql_s_t* acc1 = (ql_s_t*) calloc(acsize, sizeof(ql_s_t) );
    //acc2 = (ql_s_t*) calloc(acsize, sizeof(ql_s_t) );
    memcpy(acc1, up->frac, up->m_size * sizeof(ql_s_t));
    //memcpy(acc2 + ediff, vp->frac, vp->m_size * sizeof(s_t));

    // 減算を実行
    n_sub(acc1, up->m_size, vp->frac, vp->m_size, ediff);

    round(r, si >= 0 ? 0 : 1, up->exp, acc1, acsize);
    free( acc1 ); //free( acc2 );

    return r;
}


// @return r
// public
qlf_t* qlf_mul( qlf_t* r, const qlf_t* u, const qlf_t* v )
{
    assert( r );
    assert( u && v );

    // どちらかが0のとき
    if ( qlf_iszero(u) || qlf_iszero(v) )
        return set_zero(r);

    //       0.9999              0.1000
    //      x0.9999             x0.1000
    //      -----------         --------
    //       0.99980001          0.01000
    // acc:    ********
    const int asiz = u->m_size + v->m_size;
    ql_s_t* acc = (ql_s_t*) calloc(asiz, sizeof(ql_s_t));

    // 小数部
    for (int i = v->m_size - 1; i >= 0; --i) {
        if (!v->frac[i])
            continue;
        acc[i] += n_muladd1(acc + i + 1, u->frac, u->m_size, v->frac[i]);
    }

    round(r, u->m_sign ^ v->m_sign, u->exp + v->exp, acc, asiz);
    free(acc);

    return r;
}


// public
// r := u / v
qlf_t* qlf_div( qlf_t* r, const qlf_t* u, const qlf_t* v )
{
    assert(r && u && v);

    if ( qlf_iszero(v) )
        throw domain_error("0除算");
    if ( qlf_iszero(u) )
        return set_zero(r);

    const qlf_context* ctx = &default_ctx;

    // 商。先行する0の1桁と，丸めのための1桁だけ必要。
    ql_s_t* acc1 = (ql_s_t*) calloc(ctx->max_prec + 2, sizeof(ql_s_t));
    // 中間結果. 初期値 := 被除数 * K. 余りが残る.
    // 商を n 桁求めるのに v.size + n桁必要
    ql_s_t* acc2 = (ql_s_t*) calloc(ctx->max_prec + v->m_size + 2, sizeof(ql_s_t) );
    // 正規化した除数。
    ql_s_t* acc3 = (ql_s_t*) calloc(v->m_size, sizeof(ql_s_t) );

    // 正規化. 最上位の1回だけでよい. 余りが Kr になり, acc2 更新がぴったり.
    const ql_s_t d = BASE / (v->frac[0] + 1);
    acc2[0] = n_muladd1(acc2 + 1,
                        u->frac, min(u->m_size, ctx->max_prec + v->m_size + 1),
                        d);
    ql_s_t rrr = n_muladd1(acc3, v->frac, v->m_size, d);
    assert(!rrr);

    // 小数部
    for (int i = 0; i < ctx->max_prec + 2; ++i)
        acc1[i] = n_bdiv(acc2 + i, acc3, v->m_size);

    round(r, u->m_sign ^ v->m_sign, u->exp - v->exp + 1, acc1, ctx->max_prec + 2);
    free(acc1); free(acc2); free(acc3);

    return r;
}


/**
 * @return 商を返す。
 */
lfloat lfloat::operator / (const lfloat& a) const
{
    if (!a)
        throw domain_error("0除算");   // TODO: NaN / 0 => NaN にする??

    lfloat r;
    qlf_div(&r.val, &val, &a.val);
    return r; // std::move() は copy elision を阻害
}


lfloat& lfloat::operator /= (const lfloat& a)
{
    if (!a)
        throw domain_error("0除算");

    qlf_div(&val, &val, &a.val);
    return *this;
}

lfloat lfloat::operator % (const lfloat& a) const
{
    if (!a)
        throw domain_error("0除算");

    return *this - a * (*this / a);
}


lfloat& lfloat::operator %= (const lfloat& a)
{
    if (!a)
        throw domain_error("0除算");

    return *this = *this - a * (*this / a);
}


// 自身を符号反転する
// public
qlf_t* qlf_negate(qlf_t* r)
{
    if (qlf_isnan(r))
        return qlf_set_str(r, "NAN", NULL);
    if (qlf_iszero(r))
        return r;  // +0, -0 は区別しない.

    r->m_sign = r->m_sign ? 0 : 1;
    assert( r->m_sign == 0 || r->m_sign == 1);

    return r;
}


////////////////////////////////////////////////////////////////////////////
// 比較演算子

constexpr int compare_int(const int* x, const int* y) {
    return *x > *y ? +1 : (*x < *y ? -1 : 0);
}

#ifdef QLD_CONFIG_64
constexpr int compare_uint(const unsigned int* x, const unsigned int* y) {
    return *x > *y ? +1 : (*x < *y ? -1 : 0);
}
#endif // QLD_CONFIG_64


/**
 * public
 * @return    *x > *y     +1
 *            *x == *y    0
 *            *x < *y     -1
 * unordered を返却できないので、NaN はもっとも小さい値とみなす.
 */
int qlf_compare(const qlf_t* x, const qlf_t* y)
{
    assert(x && y);

    if ( qlf_isnan(x) )
        return qlf_isnan(y) ? 0 : -1;
    else if ( qlf_isnan(y) )
        return +1;

    // 符号 -> どちらかが負値
    if (x->m_sign != y->m_sign)
        return !x->m_sign ? +1 : -1;

    // 両方 0 以上か, 両方が負の値
    if ( qlf_isinf(x) )
        return qlf_isinf(y) ? 0 : (!x->m_sign ? +1 : -1);
    else if ( qlf_isinf(y) )
        return !y->m_sign ? -1 : +1;

    if ( qlf_iszero(x) )
        return qlf_iszero(y) ? 0 : !y->m_sign ? -1 : +1;
    else if ( qlf_iszero(y) )
        return !x->m_sign ? +1 : -1;

    // 0 以外の正規数
    assert(x->frac && y->frac);

    // 指数部
    int comp = compare_int(&x->exp, &y->exp);
    if (comp)
        return !x->m_sign ? comp : -comp;

    // 小数部
    for (int i = 0; i < min(x->m_size, y->m_size); i++) {
        comp = compare_uint(&x->frac[i], &y->frac[i]);
        if (comp)
            return !x->m_sign ? comp : -comp;
    }
    comp = compare_uint(&x->m_size, &y->m_size);
    if (comp)
        return !x->m_sign ? comp : -comp;

    return 0;
}


// 後置
lfloat lfloat::operator ++ (int)
{
    lfloat r = *this;
    ++*this;
    return r; // std::move() は copy elision を阻害
}

lfloat lfloat::operator -- (int)
{
    lfloat r = *this;
    --*this;
    return r; // std::move() は copy elision を阻害
}

lfloat& lfloat::operator ++()
{
    *this += lfloat("1");
    return *this;
}

lfloat& lfloat::operator --()
{
    *this -= lfloat("1");
    return *this;
}


////////////////////////////////////////////////////////////////////////////
// lf_exception

lf_exception::lf_exception(int n, const char* msg_) throw(): code(n), msg(msg_)
{
}

int lf_exception::getCode() const throw()
{
    return code;
}

const char* lf_exception::what() const throw()
{
    return msg.c_str();
}
