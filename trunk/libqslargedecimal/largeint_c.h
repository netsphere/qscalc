
#ifndef QS_LARGE_INT_C_H
#define QS_LARGE_INT_C_H

#include <stdint.h>
#include "cdefs.h"
#include "typedef.h"

#ifdef __cplusplus
extern "C" {
#endif

// 型 ///////////////////////////////////////////////////////////////////

struct qli_t
{
    // abs(len) = frac の桁数. m_size が負数のとき、値もマイナス.
    // 値が 0 のとき, m_size = 0.
    int m_size;

    unsigned alloc_size;

    // 上の桁から格納する. よくある多倍長整数は下から格納するが、浮動小数点と
    // 共用にする。
    ql_s_t* frac;
};


// 初期化・代入 /////////////////////////////////////////////////////////////

qli_t* qli_init(qli_t* r);

// メモリを解放する
void qli_clear(qli_t* r);

// 文字列で設定する。初期化を兼ねないことに注意!
qli_t* qli_set_str(qli_t* r, const char* str, const char** endptr )
    __nonnull((2));

// 初期化を兼ねる
qli_t* qli_init_set_str(qli_t* r, const char* str, const char** endptr )
    __nonnull((2));

// Copy. 初期化を兼ねない!
qli_t* qli_copy(qli_t* r, const qli_t* u);

qli_t* qli_init_copy(qli_t* r, const qli_t* u) ;


// 変換 //////////////////////////////////////////////////////////////////

char* qli_get_str(const qli_t* v, char* str, int size);

long long qli_get_int(const qli_t* v);


// 算術演算 //////////////////////////////////////////////////////////////////

// r := u + v. qli_add(x, x, x) とすると, x := x * 2.
qli_t* qli_add(qli_t* r, const qli_t* u, const qli_t* v);

// r := u - v
qli_t* qli_sub(qli_t* r, const qli_t* u, const qli_t* v);

qli_t* qli_mul(qli_t* r, const qli_t* u, const qli_t* v);

// r := u `div` v
// 0 除算の場合は, std::domain_error 例外 (logic_error 派生) を発生する。Java は ArithmeticException 例外を発生し、これは RuntimeException 派生. どうするかな?
qli_t* qli_div(qli_t* r, const qli_t* u, const qli_t* v);

// 自身を符号反転する.
qli_t* qli_negate(qli_t* r);


// 比較 //////////////////////////////////////////////////////////////////

// qsort() 用.
int qli_compare(const qli_t* x, const qli_t* y);

// 絶対値の大小を比較
// @return +1: abs(x) > abs(y)
int qli_compare_abs(const qli_t* x, const qli_t* y);


#ifdef __cplusplus
}
#endif

#endif // !QS_LARGE_INT_C_H
