﻿// -*- mode:c++; coding:utf-8-with-signature -*-

#ifndef QS_LARGE_FLOAT_C_H
#define QS_LARGE_FLOAT_C_H

#include <stdint.h>
#include <stddef.h>
#include "cdefs.h"
#include "typedef.h"

#ifdef __cplusplus
extern "C" {
#endif

// 型 ///////////////////////////////////////////////////////////////////

/**
 * 任意精度 10 進浮動小数点数
 * +0 と -0 は区別しない. 非正規化数は持たない.
 */
struct qlf_t
{
/*
  値        exp          m_size
 -----     -----------  ----------
  0        0x8000_0000  0           -- exp = 0 は大から小への途中にある.
  正規化数 それ以外     非 0
  ±INF     0x7fff_ffff  0
  NaN      0x7fff_ffff  非 0. しかし frac は NULL.
 */

    // m_sign = 0 が 0 以上, 1が負値
    unsigned m_sign :1;

    /**
     * 指数部   下駄を履かせない。
     * 値 0 かどうか判断するには, exp の値で確認しろ。
     */
    int32_t exp;

    /**
     * 小数部の値のある長さ. 0, ±INF の場合も 0.
     * 算術のときに、計算後の有効精度を決めるのは, こちらではなく max_prec.
     */
    unsigned m_size;

    unsigned alloc_size;

    // 小数部   上の桁から並べる.
    // 各桁は 0 <= x < BASE。さらに frac[0] は 0 < x <BASE
    ql_s_t* frac;
};


enum QlfRound {
    QLF_ROUND_HALF_EVEN, // to nearest, ties to even.   FE_TONEAREST
    QLF_ROUND_HALF_UP,   // to nearest, ties away from zero. 四捨五入.
    QLF_ROUND_TRUNC,     // toward 0     FE_TOWARDZERO
    QLF_ROUND_CEIL,      // toward +∞    FE_UPWARD
    QLF_ROUND_FLOOR,     // toward -∞    FE_DOWNWARD
};

// コンテキスト. TODO: スレッドローカルにすること。
struct qlf_context
{
    // s_t の有効桁数. 演算は10進で (max_prec10 + 1) 桁目を四捨五入して
    // max_prec10 桁目まで求める。
    // 精度を動的に変更できるようにする。
    unsigned prec10;

    // 格納する桁数. ceil(max_prec10 / PACK)
    // frac[] メモリは max_prec より長くなることがある。
    int max_prec;    // TODO: これ必要??

    // Rounding mode.
    QlfRound round;

    // TODO: impl.
    int error_mode;
};


void qlf_set_prec(qlf_context* ctx, int new_prec);

struct qlf_error
{
    int code;
};


// 初期化・代入 /////////////////////////////////////////////////////////////

// 初期化
qlf_t* qlf_init(qlf_t* r);

// Copy. 初期化を兼ねない!
qlf_t* qlf_copy(qlf_t* r, const qlf_t* u);

qlf_t* qlf_init_copy(qlf_t* r, const qlf_t* u);

// 文字列で設定. 初期化を兼ねない!
qlf_t* qlf_set_str(qlf_t* r, const char* str, const char** endptr )
    __nonnull((2));

qlf_t* qlf_init_set_str(qlf_t* r, const char* str, const char** endptr )
    __nonnull((2));

// メモリを解放する。
void qlf_clear(qlf_t* r);


// 変換 //////////////////////////////////////////////////////////////////

#define QLF_INFINITE_EXP 0x7fff'ffff
#define QLF_ZERO_EXP ((int32_t) 0x8000'0000)

// n.ddddde+nn の形で出力する。
// @param n_digits 出力する 10進での桁数. 0の場合, すべての桁を出力.
char* qlf_get_str(const qlf_t* v, char* s, int size, int n_digits);

double qlf_get_dbl(const qlf_t* v) ;

// NaN, ±INF のいずれでもない.
inline bool qlf_isfinite(const qlf_t* u) {
    return u->exp != QLF_INFINITE_EXP;
}

inline bool qlf_isinf(const qlf_t* u) {
    return u->exp == QLF_INFINITE_EXP && !u->m_size;
}

inline bool qlf_isnan(const qlf_t* u) {
    return u->exp == QLF_INFINITE_EXP && u->m_size;
}

inline bool qlf_iszero(const qlf_t* u) {
    return u->exp == QLF_ZERO_EXP;
}

// 負の場合, 非 0
unsigned int qlf_signbit(const qlf_t* v);


// 演算 //////////////////////////////////////////////////////////////////

qlf_t* qlf_add(qlf_t* r, const qlf_t* u, const qlf_t* v);

// r := u - v
qlf_t* qlf_sub(qlf_t* r, const qlf_t* u, const qlf_t* v);

qlf_t* qlf_mul(qlf_t* r, const qlf_t* u, const qlf_t* v);

// r := u / v
qlf_t* qlf_div(qlf_t* r, const qlf_t* u, const qlf_t* v);

// 自身を符号反転する
qlf_t* qlf_negate(qlf_t* r);


// 比較 //////////////////////////////////////////////////////////////////

// qsort() 用. NaN は, 正しくないが, -∞より小さいとみなす.
int qlf_compare(const qlf_t* x, const qlf_t* y);


#ifdef __cplusplus
}
#endif

#endif // !QS_LARGE_FLOAT_C_H
