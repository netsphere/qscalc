﻿// -*- coding:utf-8-with-signature -*-

#ifndef QS_CDEFS_H
#define QS_CDEFS_H

// /usr/include/features.h:# define __GNUC_PREREQ(maj, min)

#ifndef __GNUC_PREREQ
// clang でも __GNUC__, __GNUC_MINOR__ が定義される
#if defined __GNUC__ && defined __GNUC_MINOR__
  #define __GNUC_PREREQ(maj, min) \
        ((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))
#else
  #define __GNUC_PREREQ(maj, min) 0
#endif
#endif

// /usr/include/sys/cdefs.h:# define __glibc_has_attribute(attr) __has_attribute (attr)
// __has_attribute は GCC拡張.

/* The nonnull function attribute marks pointer parameters that
   must not be NULL.  This has the name __nonnull in glibc,
   and __attribute_nonnull__ in files shared with Gnulib to avoid
   collision with a different __nonnull in DragonFlyBSD 5.9.  */
#ifndef __attribute_nonnull__
  #if __GNUC_PREREQ(3,3)
#  define __attribute_nonnull__(params) __attribute__ ((__nonnull__ params))
# else
#  define __attribute_nonnull__(params)
# endif
#endif
#ifndef __nonnull
# define __nonnull(params) __attribute_nonnull__ (params)
#endif

// clang のほうが次のように書けて分かりやすい
//     int fetch(int * _Nonnull ptr);

#endif // !QS_CDEFS_H
