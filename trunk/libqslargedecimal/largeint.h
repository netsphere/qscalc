
#ifndef QS_LARGEINT_H
#define QS_LARGEINT_H

#include "largeint_c.h"
#include <stdint.h>
#include <string>

// 多倍長整数
class LargeInt
{
    qli_t val;

public:
    // 構築子・破壊子 /////////////////////////////////////////////////////

    // 初期化
    constexpr LargeInt() noexcept:
        val {.m_size = 0, .alloc_size = 0, .frac = nullptr} {
        //qli_init(&val);
    }

    explicit LargeInt(const char* str) __nonnull((1));

    LargeInt(long long a);

    // Copy
    LargeInt(const LargeInt& a);

    // Move
    LargeInt(LargeInt&& a) noexcept: val(a.val) {
        qli_init(&a.val);
    }

    virtual ~LargeInt();


    // 代入演算子 ////////////////////////////////////////////////////////

    LargeInt& operator = (const char* str) __nonnull((1)) {
        qli_set_str(&val, str, NULL); return *this;
    }

    // Copy
    LargeInt& operator = (const LargeInt& a) {
        if (this != &a) qli_copy(&val, &a.val);
        return *this;
    }

    // Move
    LargeInt& operator = (LargeInt&& a) noexcept {
        if (this != &a) {
            val = a.val; qli_init(&a.val);
        }
        return *this;
    }

    LargeInt& operator += (const LargeInt& a)   {
        qli_add(&val, &val, &a.val); return *this; }

    LargeInt& operator -= (const LargeInt& a)   {
        qli_sub(&val, &val, &a.val); return *this; }

    LargeInt& operator *= (const LargeInt& a)   {
        qli_mul(&val, &val, &a.val); return *this; }

    LargeInt& operator /= (const LargeInt& a);
    LargeInt& operator %= (const LargeInt& a);
    LargeInt& operator ++();
    LargeInt& operator --();
    LargeInt  operator ++(int);   // 後置
    LargeInt  operator --(int);


    // 算術演算子 ////////////////////////////////////////////////////////

    LargeInt operator + (const LargeInt& a) const   {
        LargeInt r; qli_add(&r.val, &val, &a.val);
        return r; // std::move() は copy elision を阻害.
    }

    LargeInt operator - (const LargeInt& a) const   {
        LargeInt r; qli_sub(&r.val, &val, &a.val);
        return r; // std::move() は copy elision を阻害.
    }

    LargeInt operator * (const LargeInt& a) const   {
        LargeInt r; qli_mul(&r.val, &val, &a.val);
        return r; // std::move() は copy elision を阻害.
    }

    LargeInt operator / (const LargeInt& a) const;
    LargeInt operator % (const LargeInt& a) const;

    // 単項マイナス
    LargeInt operator - () const      {
        LargeInt r(*this); qli_negate(&r.val);
        return r; // std::move() は copy elision を阻害.
    }


    // 変換 //////////////////////////////////////////////////////////////

    std::string get_str() const;

    // Note. explicit を付けないと, int への暗黙の型変換で謎挙動。
    explicit operator bool() const noexcept { return val.m_size; }

    // 論理否定
    //bool operator ! () const noexcept        { return qli_iszero(&val); }

    const qli_t* get_cval() const noexcept { return &val; }


    // 比較 //////////////////////////////////////////////////////////////

    // qsort() 用
    //static int compare(const LargeInt* x, const LargeInt* y) noexcept;

#if __cplusplus >= 202002L  // C++20
    const std::strong_ordering& operator <=>(const LargeInt& a) const noexcept {
        return qli_compare(&val, &a.val);
    }
#else
    // NaN == x -> false
    bool operator == (const LargeInt& a) const noexcept   {
        return !qli_compare(&val, &a.val);  }

    // NaN != x -> false. !(x == y) と x != y が一致しない
    bool operator != (const LargeInt& a) const noexcept {
        return qli_compare(&val, &a.val) != 0;   }

    // !(x < y) と x >= y が一致しない!
    bool operator <  (const LargeInt& a) const noexcept   {
        return qli_compare(&val, &a.val) < 0;    }

    bool operator <= (const LargeInt& a) const noexcept   {
        return qli_compare(&val, &a.val) <= 0;   }

    bool operator >  (const LargeInt& a) const noexcept   {
        return qli_compare(&val, &a.val) > 0; }

    bool operator >= (const LargeInt& a) const noexcept   {
        return qli_compare(&val, &a.val) >= 0; }
#endif // __cplusplus >= 202002L

};

#endif // !QS_LARGEINT_H
