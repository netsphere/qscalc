// 多倍長整数

// ググるとここが上位に出てくるが、そのままでは実装できない。
//     超高速！多倍長整数の計算手法【前編：大きな数の四則計算を圧倒的な速度で！】
//     https://qiita.com/square1001/items/1aa12e04934b6e749962

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <algorithm>
#include <stdexcept>  // `runtime_error`
#include "largeint.h"
#include "large_priv.h"
using namespace std;


// 初期化・代入 /////////////////////////////////////////////////////////////

// @param str が NULL の場合, errno に EINVAL を設定し, r を 0 にする.
qli_t* qli_set_str(qli_t* r, const char* str, const char** endptr )
{
    assert( r );

    // 浅い初期化
    r->m_size = 0; // alloc_size = 0; frac = nullptr;

    if ( !str ) {
        errno = EINVAL;
        if (endptr) *endptr = str;
        return r;
    }

    int sign = 0;
    const char* p = str;

    // 符号
    while ( *p == ' ' || *p == '\t' ) p++;
    if (*p == '-') {
        sign = -1; p++;
    }
    else {
        sign = +1;
        if (*p == '+') p++;
    }

    if ( !isdigit(*p) ) {
        if (endptr) *endptr = p;
        return r;
    }

    while (*p == '0' || *p == '_' || *p == '\'' ) p++;
    if ( !(*p >= '1' && *p <= '9') ) {
        if (endptr ) *endptr = p;
        return r; // 値 0 or 不正
    }

    // 1の位のほうをピッタリにするため、最後まで読みきる.
    const char* start = p;
    // 切り上げる. ceil()
    const unsigned flen = (strlen(p) + (PACK - 1)) / PACK;  // TODO: 後ろにゴミが付いていないか確認
    if (flen > r->alloc_size) {
        r->alloc_size = flen + 1;
        free(r->frac);
        r->frac = (ql_s_t*) calloc(r->alloc_size, sizeof(ql_s_t));
    }

    int n = strlen(start) % PACK; if (!n) n = PACK;
    ql_s_t* rp;
    for ( rp = r->frac; *p; n = PACK, ++rp ) {
        for ( int i = n - 1; i >= 0; --i ) {
            if (isdigit(*p)) {
                *rp = *rp * 10 + (*p - '0');
                p++;
            }
            else { // error
                if (endptr) *endptr = p;
                r->m_size = 0;
                return r;
            }
        }
    }

    r->m_size = sign * (rp - r->frac);

    return r;
}


qli_t* qli_init(qli_t* r) {
    memset(r, 0, sizeof(qli_t));
    return r;
}

void qli_clear(qli_t* r) {
    r->m_size = 0;
    r->alloc_size = 0; free(r->frac); r->frac = NULL;
}

qli_t* qli_init_set_str(qli_t* r, const char* str, const char** endptr) {
    qli_init(r);
    return qli_set_str(r, str, endptr);
}

qli_t* qli_copy(qli_t* r, const qli_t* u) {
    r->m_size = u->m_size;
    r->alloc_size = u->alloc_size;
    free(r->frac);
    r->frac = (ql_s_t*) malloc(r->alloc_size * sizeof(ql_s_t*) );
    memcpy(r->frac, u->frac, r->alloc_size * sizeof(ql_s_t));

    return r;
}

qli_t* qli_init_copy(qli_t* r, const qli_t* u) {
    qli_init(r);
    return qli_copy(r, u);
}

LargeInt::LargeInt(const char* str)
{
    qli_init_set_str(&val, str, NULL);
}

LargeInt::LargeInt(long long a)
{
    // 2^64 = 18_446_744_073_709_551_616 (20桁)
    char s[21];
    sprintf(s, "%lld", a);
    qli_init_set_str(&val, s, NULL);
}


LargeInt::~LargeInt() {
    qli_clear(&val);
}


// 変換 //////////////////////////////////////////////////////////////////

static bool IS_ZERO(const qli_t* u) {
    return !u->m_size;
}

// public
char* qli_get_str(const qli_t* val, char* str, int size)
{
    assert( val && str && size > 0 );

    if ( IS_ZERO(val) )
        return strcpy(str, "0");

    // 先頭から埋めていくだけでよい.
    // itoa() は非標準で, Linux にはない。
    char* p = str;
    if (val->m_size < 0)
        *(p++) = '-';

    for (int i = 0; i < abs(val->m_size); ++i) {
#ifdef QLD_CONFIG_64
        p += sprintf(p, i == 0 ? "%llu" : CFMT, val->frac[i]);
#else
        p += sprintf(p, i == 0 ? "%u" : CFMT, val->frac[i]);
#endif
    }

    return str;
}

string LargeInt::get_str() const {
    if (!val.m_size)
        return "0";

    int siz = abs(val.m_size) * PACK + 2;  // '-', '\0'
    char* s = (char*) calloc(siz, sizeof(ql_s_t));
    qli_get_str(&val, s, siz * sizeof(ql_s_t));
    string ret(s); free(s);

    return ret;
}


// 演算 //////////////////////////////////////////////////////////////////

static int sign(const qli_t* v) {
    return v->m_size > 0 ? +1 : (v->m_size < 0 ? -1 : 0);
}


// r := u + v. qli_add(u, u, v) とすると, u += v.
// public
qli_t* qli_add(qli_t* r, const qli_t* u, const qli_t* v)
{
    assert( r && u && v );

    // どちらかが 0 の時
    if ( IS_ZERO(u) ) return qli_copy(r, v);
    if ( IS_ZERO(v) ) return qli_copy(r, u);

    // u, v の符号が異なるときは, 減算を呼ぶ
    if ( sign(u) != sign(v) ) {
        qli_t vn; qli_copy(qli_init(&vn), v);
        qli_sub(r, u, qli_negate(&vn));
        qli_clear(&vn);
        return r;
    }

    // (uの桁数) >= (vの桁数) として、差を ediff に.
    const qli_t *up, *vp;
    if ( abs(u->m_size) >= abs(v->m_size) ) {
        up = u; vp = v; }
    else {
        up = v; vp = u; }
    int ediff = abs(up->m_size) - abs(vp->m_size);

    // qli_add(x, x, y) で |x| < |y| がありうるので、結果用メモリ確保は必要.
    const unsigned acsize = max(abs(up->m_size), abs(vp->m_size) + ediff);
    ql_s_t* acc1 = (ql_s_t*) calloc(acsize + 1, sizeof(ql_s_t));
    memcpy( acc1 + 1, up->frac, abs(up->m_size) * sizeof(ql_s_t) );

    // 加算実行
    acc1[0] = n_add(acc1 + 1, abs(up->m_size), vp->frac, abs(vp->m_size), ediff);

    // r に書き戻す
    if (acc1[0]) { // 最上位に繰り上がり. swap する.
        r->m_size = sign(up) * (acsize + 1);
        r->alloc_size = acsize + 1;
        free(r->frac); r->frac = acc1; acc1 = NULL;
    }
    else {
        if (acsize > r->alloc_size) {
            r->alloc_size = acsize + 1;   // TODO: 1.5倍とかのほうがいいか?
            free(r->frac);
            r->frac = (ql_s_t*) calloc(r->alloc_size, sizeof(ql_s_t));
        }
        r->m_size = sign(up) * acsize;
        memcpy(r->frac, acc1 + 1, acsize * sizeof(ql_s_t) );
    }
    free(acc1);

    return r;
}


// public
qli_t* qli_sub( qli_t* r, const qli_t* u, const qli_t* v )
{
    assert( r && u && v );

    // どちらかが 0
    if ( IS_ZERO(u) ) {
        qli_copy(r, v); r->m_size = -r->m_size;
        return r;
    }
    if ( IS_ZERO(v) ) return qli_copy(r, u);

    // u, v の符号が異なるときは, 加算に投げる
    if ( sign(u) != sign(v) ) {
        qli_t vn; qli_copy(qli_init(&vn), v);
        qli_add(r, u, qli_negate(&vn));
        qli_clear(&vn);
        return r;
    }

    // 符号は同じ。|u| >= |v| とする
    const qli_t *up, *vp;
    int si = 0;
    switch ( qli_compare(u, v) ) {
    case +1:  // u > v
        up = u->m_size > 0 ? u : v; vp = u->m_size > 0 ? v : u;
        si = +1;
        break;
    case 0:
        r->m_size = 0; return r;
    case -1:
        up = u->m_size > 0 ? v : u; vp = u->m_size > 0 ? u : v;
        si = -1;
        break;
    default:
        abort();
    }

    const int ediff = abs(up->m_size) - abs(vp->m_size);
    const unsigned acsize = abs(up->m_size);  // 整数ではお尻が揃う
    ql_s_t* acc1 = (ql_s_t*) calloc(acsize, sizeof(ql_s_t) );
    memcpy(acc1, up->frac, abs(up->m_size) * sizeof(ql_s_t) );

    // 実行!
    n_sub(acc1, abs(up->m_size), vp->frac, abs(vp->m_size), ediff);

    // 桁が減った分を詰める.
    unsigned i;
    for ( i = 0; i < acsize; ++i) {
        if (acc1[i]) break;
    }

    if ( !i ) {  // 桁数そのままなら swap
        r->m_size = si * abs(up->m_size);
        r->alloc_size = acsize;
        free(r->frac); r->frac = acc1; acc1 = NULL;
    }
    else {   // そうでないなら copy する
        if (acsize - i > r->alloc_size) {
            r->alloc_size = (acsize - i) + 1;
            free(r->frac);
            r->frac = (ql_s_t*) calloc(r->alloc_size, sizeof(ql_s_t));
        }
        r->m_size = si * (acsize - i);
        memcpy(r->frac, acc1 + i, (acsize - i) * sizeof(ql_s_t) );
    }
    free(acc1);

    return r;
}


/**
 * r := u * v
 * @return r
 */
qli_t* qli_mul( qli_t* r, const qli_t* u, const qli_t* v )
{
    assert( r );
    assert( u && v );

    // どちらかが 0
    if ( IS_ZERO(u) || IS_ZERO(v) ) {
        r->m_size = 0; return r;
    }

    const int N = abs(u->m_size) + abs(v->m_size); // N >= 2
    ql_s_t* acc = (ql_s_t*) calloc(N, sizeof(ql_s_t) );

/*
          9 9 9
      x   9 0 9
     ----------
        8 9 9 1   -> k = 8
acc * * * * * *
*/
    ql_s_t k = 0;
    for (int i = abs(v->m_size) - 1; i >= 0; --i ) {
        if ( !v->frac[i] )
            continue;
        k = n_muladd1( acc + i + 1, u->frac, abs(u->m_size), v->frac[i] );
        acc[i] += k;
    }

    if (k) { // 最上位に繰り上がり. swap すればよい。
        r->m_size = sign(u) * sign(v) * N;
        r->alloc_size = N;
        free(r->frac); r->frac = acc; acc = NULL;
    }
    else { // Copy する
        if (N - 1 > r->alloc_size) {
            r->alloc_size = N + 1;   // TODO: 1.5倍とかのほうがいいか?
            free(r->frac);
            r->frac = (ql_s_t*) calloc(r->alloc_size, sizeof(ql_s_t));
        }
        r->m_size = sign(u) * sign(v) * (N - 1);
        memcpy(r->frac, acc + 1, (N - 1) * sizeof(ql_s_t) );
    }
    free(acc);

    return r;
}

/*
    888
  x 888
 -------
   7104
  7104
 7104
--------
    544

         9  9  9
       x 9  9  9
      -----------
      8  9  9  1
   8  9  9  1
8  9  9  1
 ----------------
   *  * 19 10  1   <- 下のほうからの繰り上がりがあるので、計算は必要.
*/


/*
商q の桁数は,
    桁数(u) - 桁数(v) + (uの上位 v桁 >= v ? +1 : 0)
 999 / 1 -> q = 999
 999 / 8 -> q = 124 + 7/8
 999 / 9 -> 111
 100 / 1 -> 100
 100 / 9 ->  11 + 1/9

 9999 / 10 -> q = 999 + 9/10
 1000 / 99 -> q =  10 + 10/99
*/

// r := u `div` v
qli_t* qli_div( qli_t* r, const qli_t* u, const qli_t* v )
{
    assert( r );
    assert( u && v );

    if ( IS_ZERO(v) )
        throw domain_error("divide by zero");
    if ( IS_ZERO(u) || qli_compare_abs(u, v) < 0 ) {
        // TODO: 余りがある場合, 被除数 u がマイナス, 除数 v がプラスの時は、
        //       商 := 商 - 1 すること.
        // See http://www.gem.hi-ho.ne.jp/joachim/floorandceiling/remainder.html
        //     この「4. 除数と同符号の剰余」がよい.
        r->m_size = 0; return r;
    }

    // (u `div` v) の桁数は 1以上.
    const unsigned acc_siz = abs(u->m_size) - abs(v->m_size) + 1;

    // 商を順に格納していく
    ql_s_t* acc1 = (ql_s_t*) calloc(acc_siz, sizeof(ql_s_t));
    // 中間結果. 初期値 := 被除数 * K. 上位から余りに書き換えて、崩していく.
    // -> これが余り kr になる。
    ql_s_t* acc2 = (ql_s_t*) calloc( abs(u->m_size) + 1, sizeof(ql_s_t) );
    // 除数 * K
    ql_s_t* acc3 = (ql_s_t*) calloc( abs(v->m_size), sizeof(ql_s_t) );

    // u, v の両方に整数 K を掛ける. これで q1 - q が高々 2になる。
    // 各桁の余りは Kr になる。
    const ql_s_t K = BASE / (v->frac[0] + 1);
    acc2[0] = n_muladd1(acc2 + 1, u->frac, abs(u->m_size), K);
    ql_s_t r0 = n_muladd1(acc3, v->frac, abs(v->m_size), K);
    assert(!r0);

    for (unsigned i = 0; i < acc_siz; ++i)
        acc1[i] = n_bdiv(acc2 + i, acc3, abs(v->m_size));

    // 頭が 0 のケースがある。
    if (acc1[0]) { // ぴったり。swapする
        r->m_size = sign(u) * sign(v) * acc_siz;
        r->alloc_size = acc_siz;
        free(r->frac); r->frac = acc1; acc1 = NULL;
    }
    else { // Copyする
        if ( acc_siz - 1 > r->alloc_size) {
            r->alloc_size = acc_siz + 1;
            free(r->frac);
            r->frac = (ql_s_t*) calloc(r->alloc_size, sizeof(ql_s_t));
        }
        r->m_size = sign(u) * sign(v) * (acc_siz - 1);
        memcpy(r->frac, acc1 + 1, (acc_siz - 1) * sizeof(ql_s_t));
    }
    free(acc1); free(acc2); free(acc3);

    return r;
}


/**
 * @return 商を返す。
 */
LargeInt LargeInt::operator / (const LargeInt& a) const
{
    if (!a)
        throw domain_error("0除算");

    LargeInt r;
    qli_div(&r.val, &val, &a.val);
    return r;  // std::move() は copy elision を阻害
}


LargeInt& LargeInt::operator /= (const LargeInt& a)
{
    if (!a)
        throw domain_error("0除算");

    qli_div(&val, &val, &a.val);
    return *this;
}

LargeInt LargeInt::operator % (const LargeInt& a) const
{
    if (!a)
        throw domain_error("0除算");

    return *this - a * (*this / a);
}


LargeInt& LargeInt::operator %= (const LargeInt& a)
{
    if (!a)
        throw domain_error("0除算");

    return *this = *this - a * (*this / a);
}

// 自身を符号反転する
// public
qli_t* qli_negate(qli_t* r)
{
    if (IS_ZERO(r))
        return r;  // +0, -0 は区別しない.

    r->m_size = -r->m_size;
    return r;
}


////////////////////////////////////////////////////////////////////////////
// 比較演算子

// public
int qli_compare_abs(const qli_t* u, const qli_t* v)
{
    assert( u && v );

    if ( abs(u->m_size) != abs(v->m_size) )
        return abs(u->m_size) > abs(v->m_size) ? +1 : -1;

    if ( IS_ZERO(u) ) return 0;  // 両方 0

    // 上位の桁から見ていく
    for (int i = 0; i < abs(u->m_size); ++i) {
        if (u->frac[i] != v->frac[i])
            return u->frac[i] > v->frac[i] ? +1 : -1;
    }

    return 0;
}


// public
int qli_compare(const qli_t* x, const qli_t* y)
{
    assert( x && y);

    // 0 がからむ場合も。
    if (sign(x) != sign(y) )
        return sign(x) - sign(y);

    int comp = qli_compare_abs(x, y);
    return comp ? (sign(x) > 0 ? comp : -comp) : 0;
}
