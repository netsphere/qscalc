﻿// -*- coding:utf-8-with-signature -*-
// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#ifndef QSCALC_MATH_LF
#define QSCALC_MATH_LF

#include <stdlib.h>
#include "lfloat.h"

// 算術関数
extern lfloat lf_abs(const lfloat& x);  // 絶対値
extern lfloat lf_eps(const lfloat& x);  // イプシロン
extern lfloat lf_max(const lfloat& x, const lfloat& y);
extern lfloat lf_min(const lfloat& x, const lfloat& y);
sgn() 符号 sign
extern lfloat lf_sqr(const lfloat& x);


extern lfloat lf_ceil(const lfloat& x);
extern lfloat lf_fp(const lfloat& x);
extern lfloat lf_maxnum();
extern lfloat lf_mod(const lfloat& x, const lfloat& y);
extern lfloat lf_remainder(const lfloat& x, const lfloat& y);
extern lfloat lf_round(const lfloat& x, const lfloat& n);
extern lfloat lf_truncate(const lfloat& x, const lfloat& n);
extern lfloat lf_int(const lfloat& x);
extern lfloat lf_ip(const lfloat& x);

// 指数・対数
extern lfloat lf_cosh(const lfloat& x);
extern lfloat lf_exp(const lfloat& x);
extern lfloat lf_sinh(const lfloat& x);
extern lfloat lf_tanh(const lfloat& x);

// 角度
extern lfloat lf_angle(const lfloat& x, const lfloat& y);
extern lfloat lf_deg(const lfloat& x);
extern lfloat lf_rad(const lfloat& x);

// 三角関数
extern lfloat lf_arccos(const lfloat& x);
extern lfloat lf_arcsin(const lfloat& x);
extern lfloat lf_arctan(const lfloat& x);
extern lfloat lf_cos(const lfloat& x);
extern lfloat lf_cot(const lfloat& x);
extern lfloat lf_cosec(const lfloat& x);
extern lfloat lf_sec(const lfloat& x);
extern lfloat lf_sin(const lfloat& x);
extern lfloat lf_tan(const lfloat& x);

extern lfloat lf_pi();

// 乱数
extern lfloat lf_rnd();

// 日付・時間
extern lfloat lf_date();
extern lfloat lf_time();

#endif
