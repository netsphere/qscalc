﻿// -*- mode:c++; coding:utf-8-with-signature -*-

// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#ifndef QS_LARGE_FLOAT
#define QS_LARGE_FLOAT

#include "lfloat_c.h"
#include <exception>
#include <string>

/**
 * 任意精度 10 進浮動小数点数
 * +0 と -0 は区別しない. 非正規化数は持たない.
 */
class lfloat
{
    qlf_t val;

public:
    // 構築子・破壊子 /////////////////////////////////////////////////////

    constexpr lfloat() noexcept:
    val {.m_sign = 0, .exp = QLF_ZERO_EXP, .m_size = 0, .alloc_size = 0, .frac = nullptr} {
    }

    // 初期化
    explicit lfloat(const char* s ) __nonnull((1));

    explicit lfloat(const std::string& str) ;

    // 初期化.
    lfloat(double a);

    // Copy
    lfloat(const lfloat& a);

    // Move
    lfloat(lfloat&& a) noexcept: val(a.val) {
        qlf_init(&a.val);
    }

    virtual ~lfloat();


    // 代入演算子 ////////////////////////////////////////////////////////

    lfloat& operator = (const char* s) __nonnull((1)) {
        qlf_set_str(&val, s, NULL); return *this; }

    // Copy
    lfloat& operator = (const lfloat& a)    {
        if (this != &a) qlf_copy(&val, &a.val);
        return *this;
    }

    // Move
    lfloat& operator = (lfloat&& a) noexcept {
        if (this != &a) {
            val = a.val; qlf_init(&a.val);
        }
        return *this;
    }

    lfloat& operator += (const lfloat& a)   {
        qlf_add(&val, &val, &a.val); return *this; }

    lfloat& operator -= (const lfloat& a)   {
        qlf_sub(&val, &val, &a.val); return *this; }

    lfloat& operator *= (const lfloat& a)   {
        qlf_mul(&val, &val, &a.val); return *this; }

    lfloat& operator /= (const lfloat& a);
    lfloat& operator %= (const lfloat& a);
    lfloat& operator ++();
    lfloat& operator --();
    lfloat  operator ++(int);   // 後置
    lfloat  operator --(int);


    // 算術演算子 ////////////////////////////////////////////////////////

    lfloat operator + (const lfloat& a) const   {
        lfloat r; qlf_add(&r.val, &val, &a.val);
        return r; // std::move() は copy elision を阻害.
    }

    lfloat operator - (const lfloat& a) const   {
        lfloat r; qlf_sub(&r.val, &val, &a.val);
        return r; // std::move() は copy elision を阻害.
    }

    lfloat operator * (const lfloat& a) const   {
        lfloat r; qlf_mul(&r.val, &val, &a.val);
        return r; // std::move() は copy elision を阻害.
    }

    lfloat operator / (const lfloat& a) const;
    lfloat operator % (const lfloat& a) const;

    // 単項マイナス
    lfloat operator - () const      {
        lfloat r(*this); qlf_negate(&r.val);
        return r; // std::move() は copy elision を阻害.
    }


    // 変換 //////////////////////////////////////////////////////////////

    std::string get_str(int n_digits = 0) const;

    // DEBUG
    const qlf_t* get_cval() const noexcept { return &val; }


    // 比較 //////////////////////////////////////////////////////////////

    // NaN は, もっとも小さいとみなす。
    // C++ であれば partial_ordering::unordered を返すべきだが.
    //static int compare(const lfloat* x, const lfloat* y) noexcept;   // qsort()用

#if __cplusplus >= 202002L  // C++20
    const std::partial_ordering& operator <=>(const lfloat& a) const noexcept {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ?
                        std::partial_ordering::unordered : qlf_compare(&val, &a.val);
    }
#else
    // NaN == x -> false
    bool operator == (const lfloat& a) const noexcept   {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ? false :
                                                        !qlf_compare(&val, &a.val);
    }

    // NaN != x -> false. !(x == y) と x != y が一致しない
    bool operator != (const lfloat& a) const noexcept {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ? false :
                                                        qlf_compare(&val, &a.val) != 0;
    }

    // !(x < y) と x >= y が一致しない!
    bool operator <  (const lfloat& a) const noexcept   {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ? false :
                                                        qlf_compare(&val, &a.val) < 0;
    }

    bool operator <= (const lfloat& a) const noexcept   {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ? false :
                                                        qlf_compare(&val, &a.val) <= 0;
    }

    bool operator >  (const lfloat& a) const noexcept   {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ? false :
                                                        qlf_compare(&val, &a.val) > 0; }

    bool operator >= (const lfloat& a) const noexcept   {
        return (qlf_isnan(&val) || qlf_isnan(&a.val)) ? false :
                                                        qlf_compare(&val, &a.val) >= 0; }
#endif // __cplusplus >= 202002L

    // 論理否定
    bool operator ! () const noexcept                   {
        return qlf_isnan(&val) ? false : qlf_iszero(&val); }


    // その他 ///////////////////////////////////////////////////////////

    // 精度を10進で指定
    static void set_prec(int new_prec);

private:
    //lfloat* round(int new_sign, int new_exp, s_t u[], int u_size);

    //static s_t bdiv(s_t u[], const s_t v[], int size);

    friend lfloat lf_pow_int(const lfloat& x, const lfloat& n);
    friend lfloat lf_abs(const lfloat& x);
    friend lfloat lf_exp(const lfloat& x);
    friend lfloat lf_int(const lfloat& x);
    friend lfloat lf_ip(const lfloat& x);
    friend lfloat lf_log(const lfloat& x);
    friend lfloat lf_log10(const lfloat& x);
    friend lfloat lf_log2(const lfloat& x);
    friend lfloat lf_pow(const lfloat& x, const lfloat& y);
    friend lfloat lf_sgn(const lfloat& x);
    friend lfloat lf_sqr(const lfloat& x);
    friend lfloat lf_eps(const lfloat& x);
};


////////////////////////////////////////////////////////////////////////////
// lf_exception

class lf_exception: public std::exception
{
    int code;
    std::string msg;
public:
    lf_exception(int n, const char* msg) throw();
    int getCode() const throw();
    virtual const char* what() const throw();
};

////////////////////////////////////////////////////////////////////////////

#endif
