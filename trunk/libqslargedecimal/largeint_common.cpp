
#include "large_priv.h"
#include <assert.h>
#include <algorithm>
#include <stdexcept>
// /usr/lib/gcc/x86_64-redhat-linux/12/include/x86gprintrin.h
// /usr/lib64/clang/15.0.7/include/x86gprintrin.h
#include <x86gprintrin.h> // _addcarry_u32()
using namespace std;

#if defined(QLD_CONFIG_64) && !defined(_MSC_VER)
// VC++ は _umul128() をそのまま使う.
// @return 下位64bit
constexpr uint64_t _umul128( uint64_t x, uint64_t y, unsigned long long* highproduct )
{
    unsigned __int128 t = ((unsigned __int128) x) * y;
    *highproduct = uint64_t(t >> 64);
    return uint64_t(t);
}

// @return 64bit quotient.
constexpr uint64_t _udiv128( uint64_t highdividend, uint64_t lowdividend,
                          uint64_t divisor,
                   unsigned long long* remainder )
{
    if (!divisor)
        throw domain_error("divide by zero");

    unsigned __int128 t = (((unsigned __int128) highdividend) << 64) | lowdividend;
    *remainder = t % divisor;
    return t / divisor;
}
#endif // !_MSC_VER

#if defined(QLD_CONFIG_64) && !defined(__SIZEOF_INT128__)
struct u128 {
    ql_s_t lo, hi;
};

constexpr int cmp_uint128(const u128& left, const u128& right)
{
    int r = compare_uint(&left.hi, &right.hi);
    if (r) return r;
    return compare_uint(&left.lo, &right.lo);
}

// r := x - y
static void sub128(const u128& x, const u128& y, u128* r)
{
    // 下から引く.
    unsigned char k = _subborrow_u64(0, x.lo, y.lo, &r->lo);
    _subborrow_u64(k, x.hi, y.hi, &r->hi);
}
#endif // QLD_CONFIG_64


/**
 * u += v.
 * @param u  長さ u_size
 * @param v  長さ v_size. |u| >= |v| でなければならない
 * @param ediff v をずらす桁数. ediff >= 0
 * @return 最上位に桁溢れがあればその値 (=1), なければ 0.
 */
ql_s_t n_add( ql_s_t u[], int u_size,
                  const ql_s_t v[], int v_size, int ediff )
{
    assert( u && u_size > 0 );
    assert( v && v_size > 0 );
    assert( ediff >= 0 );

    const int N = max(u_size, v_size + ediff);

    unsigned char k = 0;
    // 先頭が高い位の桁
    for ( int i = N - 1; i >= 0; --i ) {
        ql_s_t reg;
#ifdef QLD_CONFIG_64
        unsigned char c =
            _addcarryx_u64(k, i < u_size ? u[i] : 0,
                    (i - ediff >= 0 && i - ediff < v_size) ? v[i - ediff] : 0,
                    &reg);
        k = _udiv128(c, reg, BASE, u + i);
#else
        _addcarryx_u32(k, i < u_size ? u[i] : 0,
                    (i - ediff >= 0 && i - ediff < v_size) ? v[i - ediff] : 0,
                    &reg);
        // std::div() は inline ではない。
        u[i] = reg % BASE;
        k = reg / BASE; // k = 0 or 1
#endif
    }

    return k;
}


/**
 * u -= v
 * @param u, 長さ u_size
 * @param v, 長さ v_size. |u| >= |v| でなければならない.
 */
void n_sub( ql_s_t u[], int u_size,
            const ql_s_t v[], int v_size, int ediff )
{
    assert( u && u_size > 0 );
    assert( v && v_size > 0 );
    assert( ediff >= 0 );

    const int N = max(u_size, v_size + ediff);

    // 下の桁から引いていく
    unsigned char k = 0;
    for ( int i = N - 1; i >= 0; --i ) {
#ifdef QLD_CONFIG_64
        k = _subborrow_u64(k, i < u_size ? u[i] : 0,
                    (i - ediff >= 0 && i - ediff < v_size) ? v[i - ediff] : 0,
                    u + i);
#else
        k = _subborrow_u32(k, i < u_size ? u[i] : 0,
                    (i - ediff >= 0 && i - ediff < v_size) ? v[i - ediff] : 0,
                    u + i);
#endif
        if (k) u[i] += BASE;
    }
    assert(!k); // |u| >= |v| のため.
}


/**
 * 下請け. n 桁 x 1桁
 * r += u * v
 * @param r, u: 長さ size.
 * @return 最上位の繰り上がり.
 */
ql_s_t n_muladd1(ql_s_t r[], const ql_s_t u[], int u_size, ql_s_t v)
{
    assert( r );
    assert( u && u_size > 0 );

    if (!v)
        return 0;

    ql_s_t k = 0;   // 0 <= k < BASE

    // 下の桁から積和する.
    for (int i = u_size - 1; i >= 0; --i) {
#if defined(QLD_CONFIG_64) && !defined(__SIZEOF_INT128__)
        u128 reg; //ql_s_t hi, lo;
        reg.lo = _umul128(u[i], v, &reg.hi);
        // いちいちオーバフローがありえる
        reg.hi += _addcarryx_u64(0, reg.lo, r[i], &reg.lo);
        reg.hi += _addcarryx_u64(0, reg.lo, k, &reg.lo);
        // 10進に直す
        k = _udiv128(reg.hi, reg.lo, BASE, r + i);
#else
        ql_d_t reg = r[i] + ((ql_d_t) u[i]) * v + k;
        r[i] = reg % BASE;
        k = reg / BASE;  // 0 <= k < BASE
#endif
    }

    return k;
}


/**
 * u -= v * q
 * @param u, v: 長さsize
 * @return  v * q が u から引ききれなくなった場合, 0 < 戻り値 < BASE
 *
    //      (v * q)の桁数が(size + 1)になったとき最上位，そうでないとき0
    //      (v * q)の桁数がsizeでもuから引ききれないときは借りた値。
 */
static ql_s_t n_mulsub(ql_s_t u[], const ql_s_t v[], int size,
                       const ql_s_t& q)
{
    assert(u && v && size > 0);
    if (!q)
        return 0;

    ql_s_t k = 0;  // 0 <= k < BASE

    for (int i = size - 1; i >= 0; --i ) {
#if defined(QLD_CONFIG_64) && !defined(__SIZEOF_INT128__)
        u128 reg; //ql_s_t hi, lo;
        reg.lo = _umul128(v[i], q, &reg.hi);
        reg.hi += _addcarryx_u64(0, reg.lo, k, &reg.lo);
        // ここで10進に戻す
        k = _udiv128(reg.hi, reg.lo, BASE, &reg.lo);
        unsigned char c = _subborrow_u64(0, u[i], reg.lo, u + i);
        if (c) {
            u[i] += BASE; ++k;
        }
#else
        ql_d_t reg1 = ql_d_t(v[i]) * q + k;
        k = reg1 / BASE;
        //unsigned char c = _subborrow_u32(0, u[i], reg1 % BASE, u + i);
        ql_s_t reg2 = u[i] - (reg1 % BASE);
        if (reg2 > u[i]) {
            u[i] = reg2 + BASE; ++k;
        } else
            u[i] = reg2;
        // 右辺の前半は引き算の繰り下がり. 後半は v * q の上位桁. 0 <= k < BASE
        //k = (1 - reg2 / BASE) + reg1 / BASE;
#endif
    }

    return k;
}


/**
 * u [size + 1 桁] / v [size 桁] の除算
 * @param u 長さ size + 1.  出力: u := u % v
 * @param v 長さ size. v[0] >= BASE / 2 でなければならない.
 * @return 商 floor(u / v)
 */
ql_s_t n_bdiv( ql_s_t u[], const ql_s_t v[], int size)
{
    assert( u );
    assert( v && v[0] >= BASE / 2);    // q1 - q <= 2 になる
    assert( size > 0 );

    ql_s_t quo;  // 商

#if defined(QLD_CONFIG_64) && !defined(__SIZEOF_INT128__)
    u128 d; //ql_s_t d_hi, d_lo;
    d.lo = _umul128( u[0], BASE, &d.hi);
    d.hi += _addcarryx_u64(0, d.lo, u[1], &d.lo);
#else
    const ql_d_t d = ql_d_t(u[0]) * BASE + u[1];
#endif

    if (size == 1) {
#if defined(QLD_CONFIG_64) && !defined(__SIZEOF_INT128__)
        // 余りは v[0] より小さい.
        quo = _udiv128( d.hi, d.lo, v[0], u + 1);
#else
        quo = d / v[0];         // 真の商
        u[1] = d % v[0];  // 余り
#endif
        u[0] = 0; // rem / BASE;
        return quo;
    }

    // u, vの上位から商の近似値を求める -> (q1 - q) を高々 1に抑える
#if defined(QLD_CONFIG_64) && !defined(__SIZEOF_INT128__)
    ql_s_t rem_dmy;
    quo = u[0] == v[0] ? BASE - 1 : _udiv128(d.hi, d.lo, v[0], &rem_dmy);
    //    while (true) {
        u128 left, right;
        left.lo = _umul128(v[1], quo, &left.hi);
        right.lo = _umul128(quo, v[0], &right.hi);
        sub128(d, right, &right);
        right.lo = _umul128(right.lo, BASE, &right.hi);
        right.hi += _addcarryx_u64(0, right.lo, u[2], &right.lo);
        if (cmp_uint128(left, right) > 0)
            --quo;
    //}
#else
    quo = u[0] == v[0] ? BASE - 1 : d / v[0];
    // もともと高々 2 だったので、while でなくてよい。
    if (ql_d_t(v[1]) * quo > (d - ql_d_t(quo) * v[0]) * BASE + u[2])
        --quo;
#endif

    // ここまでで quo は，真の商と等しいか，真の商より1大きいかのいずれか。
    // 実際に引けるか試して，真の商か判定する。
    if (u[0] -= n_mulsub(u + 1, v, size, quo)) {
        // quo は1大きかった
        --quo;
        u[0] += n_add(u + 1, size, v, size, 0);
        assert(!u[0]);
    }

    return quo;
}
