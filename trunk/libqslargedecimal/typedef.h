
#ifndef QS_LARGE_TYPEDEF_H
#define QS_LARGE_TYPEDEF_H

#ifdef __cplusplus
extern "C" {
#endif

// VC++ には 64bit * 64bit -> 128bit の掛け算 _umul128(), 128bit / 64bit -> 64bit 商, 64bit余り の _udiv128() がある。
// ql_s_t 型を 64bit に拡張できる。
//#undef QLD_CONFIG_64
#define QLD_CONFIG_64 1

#if defined(QLD_CONFIG_64) && defined(QLD_CONFIG_32)
  #error "Conflict 32bit and 64bit."
#endif


#ifdef QLD_CONFIG_64
typedef unsigned long long ql_s_t;
//typedef uint64_t ql_s_t;
#else
// 32bit版: 1桁の数を表す。BASE * 2 が収まること.
typedef uint32_t ql_s_t;
#endif // QLD_CONFIG_64


#ifdef __cplusplus
}
#endif

#endif // !QS_LARGE_TYPEDEF_H
