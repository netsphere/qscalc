﻿// -*- coding:utf-8-with-signature -*-

// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#include <math.h>
#include <float.h>
#include <assert.h>

#include "lf_math.h"

////////////////////////////////////////////////////////////////////////////

static const lfloat LOGe10("2.3025850929940456840179914546843642076011014886287729760333279009675726096773524802359972050895982983");
static const lfloat LOGe2(
"0.693147180559945309417232121458176568075500134360255254120680009493393621969694"
"71560586332699641868754200148102057068573368552023575813055703267075163507596193"
"07275708283714351903070386238916734711233501153644979552391204751726815749320651"
"55524734139525882950453007095326366642654104239157814952043740430385500801944170"
"64167151864471283996817178454695702627163106454615025720740248163777338963855069"
"52606683411372738737229289564935470257626520988596932019650585547647033067936544"
"32547632744951250406069438147104689946506220167720424524529612687946546193165174"
"68139267250410380254625965686914419287160829380317271436778265487756648508567407"
"76484514644399404614226031930967354025744460703080960850474866385231381816767514"
"38667476647890881437141985494231519973548803751658612753529166100071053558249879"
"41472950929311389715599820565439287170007218085761025236889213244971389320378439"
"35308877482597017155910708823683627589842589185353024363421436706118923678919237"
"23146723217205340164925687274778234453534764811494186423867767744060695626573796"
"00867076257199184734022651462837904883062033061144630073719489002743643965002580"
"93651944304119115060809487930678651588709006052034684297361938412896525565396860"
"22194122924207574321757489097706752687115817051137009158942665478595964890653058"
"46025866838294002283300538207400567705304678700184162404418833232798386349001563"
"12188956065055315127219939833203075140842609147900126516824344389357247278820548"
"62715527418772430024897945401961872339808608316648114909306675193393128904316413"
"70681397776498176974868903887789991296503619270710889264105230924783917373501229"
"84242049956893599220660220465494151061391878857442455775102068370308666194808964"
"12186807790208181588580001688115973056186676199187395200766719214592236720602539"
"59543654165531129517598994005600036651356756905124592682574394648316833262490180"
"38242408242314523061409638057007025513877026817851630690255137032340538021450190"
"15374029509942262995779647427138157363801729873940704242179972266962979939312706"
"94");

static const lfloat lf_c0("0");
static const lfloat lf_c1("1");
static const lfloat lf_c2("2");
static const lfloat lf_c4("4");
static const lfloat lf_c6("6");

////////////////////////////////////////////////////////////////////////////

// 整数乗
lfloat lf_pow_int(const lfloat& x, const lfloat& n)
{
    lfloat abs_n(lf_abs(n));
    lfloat r(lf_c1);
    lfloat d(x);

    while (abs_n != lf_c0) {
        if (lf_mod(abs_n, lf_c2) != lf_c0)
            r *= d;
        d *= d;
        abs_n = lf_int(abs_n / lf_c2);
    }
    return n.size >= 0 ? r : lf_c1 / r;
}


// xの絶対値。
lfloat lf_abs(const lfloat& x)
{
    return !x.m_sign ? x : -x;
}


lfloat lf_arccos(const lfloat& x)
    // 機能
    //      xの逆余弦（アークコサイン）。単位はラジアン。
    //      cosθ=xとなるθ
    // 入力
    //      -1 <= x <= 1
    // 戻り値の範囲
    //      0 <= acos(x) <= PI
    // acos(-1) = PI
    // acos(1) = 0
{
    if (x < -lf_c1 || x > lf_c1)
        throw lf_exception(3007, "acos(x)の実引数が-1<=x<=+1でない");

    return lf_pi() / lf_c2 - lf_arcsin(x);
}

lfloat lf_angle(const lfloat& x, const lfloat& y)
    // 機能
    //      原点と座標(x, y)とを結ぶベクトルが正のx軸となす角（偏角）。単位はラジアン。
    // 入力
    //      x = y = 0であってはならない
    // 戻り値の範囲
    //      -PI < angle(x, y) <= PI
    // 解説
    //      逆時計回りを正とする。asin(y / sqr(x ^ 2 + y ^ 2)) = acos(x / sqr(x ^ 2 + y ^ 2))と等しい。
    // 例
    //      angle(1, 1) = PI / 4
    //      angle(cos(x), sin(x)) = x
{
    if (!x && !y)
        throw lf_exception(3008, "angle(0, 0)は評価できない");

    return (lfloat) atan2(y.get_dbl(), x.get_dbl());
}

lfloat lf_arcsin(const lfloat& x)
    // 機能
    //      xの逆正弦（arc sine）。単位はラジアン。
    //      sinθ=xとなるθ
    // 入力
    //      -1 <= x <= 1
    // 戻り値の範囲
    //      -PI / 2 <= asin(x, y) <= PI / 2
    // asin(-1) = -PI / 2
    // asin(1) = PI / 2
{
    if (x < "-1.0" || x > lf_c1)
        throw lf_exception(3007, "arcsin(x)の実引数が-1<=x<=+1にない");

    lfloat d = lf_sqr(lf_c1 - x * x);
    if (!d) {
        if (x > 0)
            return lf_pi() / lf_c2;
        else
            return -lf_pi() / lf_c2;
    }
    return lf_arctan(x / d);
}

lfloat lf_ceil(const lfloat& x)
    // 機能
    //      x以上で最小の整数。ceil(x) = -int(-x)
{
    return -lf_int(-x);
}

lfloat lf_sin(const lfloat& x_)
    // 機能
    //      xの正弦（sine）。単位はラジアン。
{
    if (x_ < 0)
        return -lf_sin(-x_);
    lfloat x = lf_mod(x_, lf_pi() * lf_c2);
    if (x > lf_pi())
        return -lf_sin(x - lf_pi());
    else if (x > lf_pi() / lf_c2)
        return lf_cos(x - lf_pi() / lf_c2);
    else if (x > lf_pi() / lf_c4)
        return lf_cos(lf_pi() / lf_c2 - x);

    lfloat s = x;
    lfloat xx = x * x;
    lfloat k = -x * xx;
    lfloat b = lfloat("3") * lf_c2;
    lfloat i = lf_c4;
    lfloat last;
    do {
        last = s;
        s = s + k / b;
        k = -k * xx;
        b = b * i * (i + lf_c1);
        i += lf_c2;
    } while (last != s);

    return s;
}

lfloat lf_cos(const lfloat& x_)
    // 機能
    //      xの余弦（コサイン）。単位はラジアン。
{
    lfloat x = lf_mod(lf_abs(x_), lf_pi() * lf_c2);
    if (x > lf_pi())
        return -lf_cos(x - lf_pi());
    else if (x > lf_pi() / lf_c2)
        return -lf_sin(x - lf_pi() / lf_c2);
    else if (x > lf_pi() / lf_c4)
        return lf_sin(lf_pi() / lf_c2 - x);

    lfloat s = lf_c1;
    lfloat xx = x * x;
    lfloat k = -xx;
    lfloat b = lf_c2;
    lfloat i = "3";
    lfloat last;
    do {
        last = s;
        s = s + k / b;
        k = -k * xx;
        b = b * i * (i + lf_c1);
        i += lf_c2;
    } while (last != s);

    return s;
}

lfloat lf_cosh(const lfloat& x)
    // 機能
    //      xの双曲線余弦（ハイパーボリック・コサイン）。単位はラジアン。
{
    return (lf_exp(x) + lf_exp(-x)) / lf_c2;
}

lfloat lf_cot(const lfloat& x)
    // 機能
    //      xの余接(コタンジェント）。単位はラジアン。cot(x) = cos(x) / sin(x)
{
    return lf_cos(x) / lf_sin(x);
}

lfloat lf_cosec(const lfloat& x)
    // 機能
    //      xの余割（cosecant）。単位はラジアン。
{
    return lf_c1 / lf_sin(x);
}

lfloat lf_date()
    // 機能
    //      現在の日付。十進でyyyyddd。dddは年の中の通日。
    // 例
    //      今日の日付が1997.5.9のとき，date() = 1997129
    // 解説
    //      JIS Full BASICではyydddとなっているが，2000年を越えられないので，変更した。
{
    return "-1.0";
}

lfloat lf_deg(const lfloat& x)
    // 機能
    //      ラジアン->度。x * 180 / PI
{
    return x * "180.0" / lf_pi();
}

lfloat lf_eps(const lfloat& x)
    // 機能
    //      イプシロン。x + ε != xとなる最小値。指数部の大小によって値が異なることに注意。
{
    lfloat retval = lf_c1;
    if (!x) {
        throw lf_exception(10, "eps(0)は評価できない");
            // TODO: 実装する
    }
    retval.exp = x.exp - (lfloat::max_prec - 1);
    return retval;
}


// 指数関数。自然対数の底eのx乗。
// 解説
//      exp(1) = 2.718...
//      級数展開すると，e^x = 1 + x/1! + x^2/2! + x^3/3! + ...
lfloat lf_exp(const lfloat& x)
{
#if 1
    // 級数展開版
    lfloat e, a, i, prev, k, t;

    // 次の展開式は引数が0の近くにあるとき速い。
    // そこでx = k * loge10 + t, |t| <= 1/2 * loge10とし，exp(x) = exp(t) * 10 ^ kとする
    k = lf_int(x / LOGe2 + "0.5");
    t = x - k * LOGe2;
    if (t.size >= 0) {
        e = t + lf_c1; // 解
        a = t;          // 各項
        i = lf_c2;      // カウンタ
        do {
            prev = e;   // ここまでの結果
            e += (a *= t / i);
            i++;
        } while (e != prev);
    }
    else {
        // 引数が負のとき単に展開式に代入すると，正の項と負の項が交互に現れるため桁落ちが生じる。
        // e^(-x) = 1/e^xを利用する。
        e = -t + lf_c1;
        a = -t;
        i = lf_c2;
        do {
            prev = e;
            e += (a *= -t / i);
            i++;
        } while (e != prev);
        e = lf_c1 / e;
    }
    return lf_pow_int(lf_c2, k) * e;
#else
    // 連分数版（あらかじめ収束回数が分かっているとき）
    ASSERT(lf_base == 10 && lf_prec == 100);

    lfloat xx, w, i, k, t, N("162");    // N = 4n + 2（ただしnは自然数）

    // 収束を早くするため，kとtに分離する。
    k = lf_int(x / LOGe10 + "0.5");
    t = x - k * LOGe10;     // |t| <= 1/2 * LOGe10

    // exp(t)を求める
    xx = t * t;
    w = xx / N;
    for (i = N - lf_c4; i >= lf_c6; i -= lf_c4)
        w = xx / (w + i);

    return lfloat((w + t + lf_c2) / (w - t + lf_c2), k);

/*
    // 連分数（収束を調べながら計算する方法）
    lfloat p0, p1, q0, q1, tmp;
    lfloat u, xx;

    p0 = lf_c1;        // P[-1] = 1
    q0 = lf_c0;     // Q[-1] = 0
    p1 = "2";       // P[0] = b[0]  分母
    q1 = lf_c1;     // Q[0] = 1
    u = "6";
    xx = x * x;

    lfloat i(lf_c1);
    char is[200];

    do {
        // P[k] = P[k - 2] * a[k] + P[k - 1] * b[k]
        // Q[k] = Q[k - 2] * a[k] + Q[k - 1] * b[k]
        //    分母          分子
        tmp = p1 * u +      p0 * xx; p0 = p1; p1 = tmp;
        tmp = q1 * u +      q0 * xx; q0 = q1; q1 = tmp;
        u += "4";
        i++;
    } while (p1/q1 != p0/q0);

    TRACE("n = %s\n", lfloat::get_str(is, &i));

    return (p1/q1 + x) / (p1/q1 - x);
*/
#endif
}

lfloat lf_fp(const lfloat& x)
    // xの小数部。x - ip(x)
{
    return x - lf_ip(x);
}

lfloat lf_int(const lfloat& x)
    // xを超えない最大の整数。Ｌｘ」
{
    if (!x.size)
        return x;

    // 0 < |r| < 1のとき，単にfrac[]を0にすると内部表現に狂いが生じる。
    if (x.exp <= 0)
        return x.size >= 0 ? lf_c0 : -lf_c1;

    if (abs(x.size) == x.exp) {
        // 整数
        return x;
    }

    // 小数点以下がある
    lfloat r(x);
    if (r.size > 0) {
        r.size = r.exp;
        return r;
    }
    else {
        r.size = -r.exp;
        return r - lf_c1;
    }
}

lfloat lf_ip(const lfloat& x)
    // xの整数部。sgn(x) * int(abs(x))
    // 内部表現が絶対値なので，こちらの方がlf_int()より速い。
{
    if (!x.size)
        return x;

    // 0 < |r| < 1のとき
    if (x.exp <= 0)
        return lf_c0;

    if (abs(x.size) == x.exp)
        return x;

    lfloat r(x);
    r.size = r.size > 0 ? r.exp : -r.exp;
    return r;
}

void frexp2(const lfloat& x, lfloat& t, lfloat& k)
    // x = 2^k * tとなるt, kを求める
    // log()で使いやすいようにtを1の近くにする。
{
    static const lfloat dd("1e10"), tt("1024");
    static const lfloat SQR2 = lf_sqr(lf_c2);
    static const lfloat SQR0_5 = lf_sqr("0.5");

    lfloat e(lf_c1), n(x);
    k = lf_c0;

    while (n > dd) {
        // 大きな数に対しては，大胆に除していく。
        e *= tt;
        k += "10";
        n /= tt;
    }
    if (x > SQR2) {
        while (n > SQR2) {
            e *= lf_c2;
            k++;
            n /= lf_c2;
        }
    }
    else if (x < SQR0_5) {
        while (n < SQR0_5) {
            e /= lf_c2;
            k--;
            n *= lf_c2;
        }
    }
    t = x / e;
}

lfloat lf_log(const lfloat& x)
    // 機能
    //      xの自然対数
    // 入力
    //      x > 0
{
    lfloat k, t;
    lfloat u, uu, i, r, prev;

    if (x.size <= 0)
        throw lf_exception(3004, "log()の実引数が0か負");
    if (x == lf_c1)
        return lf_c0;

    // 次の展開式は引数が1に近いとき非常に速い。
    // そこでx = 2^k * t, ただしsqr(0.5) <= t <= sqr(2)とする。
    frexp2(x, t, k);

    // 級数展開版
    u = (t - lf_c1) / (t + lf_c1);
    uu = u * u;
    i = lf_c1;
    r = u;
    do {
        i += lf_c2;
        prev = r;
        r += (u *= uu) / i;
    } while (r != prev);

    return k * LOGe2 + r * lf_c2;
}

lfloat lf_log10(const lfloat& x)
    // 機能
    //      xの常用対数
    // 入力
    //      x > 0
{
    if (x.size <= 0)
        throw lf_exception(3004, "log10()の実引数が0か負");

    return lf_log(x) / LOGe10;
}

lfloat lf_log2(const lfloat& x)
    // 機能
    //      2を底とするxの対数
    // 入力
    //      x > 0
{
    if (x.size <= 0)
        throw lf_exception(3004, "log2()の実引数が0か負");

    return lf_log(x) / LOGe2;
}

lfloat lf_max(const lfloat& x, const lfloat& y)
    // xとyの大きい方の値
{
    return x >= y ? x : y;
}

lfloat lf_maxnum()
    // 処理系が表すことのできる有限の最大の整数
{
  return (lfloat) DBL_MAX;
}

lfloat lf_min(const lfloat& x, const lfloat& y)
    // xとyの小さいほうの値
{
    return x <= y ? x : y;
}

lfloat lf_mod(const lfloat& x, const lfloat& y)
    // 機能
    //      yを法とするxの値。x - y * int(x / y)
    // 入力
    //      y != 0
{
    if (!y)
        throw lf_exception(3006, "mod()の除数が0");

    return x - y * lf_int(x / y);
}

lfloat lf_pi()
    // 機能
    //      円周率。
    // 解説
    //      PI = 4 * atn(1)
{
    static lfloat PI = lf_c0;

    if (PI != lf_c0)
        return PI;

    lfloat a, b, s, t, prev;

    a = lf_c1;
    b = lf_c1 / lf_sqr(lf_c2);
    s = lf_c1;
    t = lf_c4;
    do {
        prev = a;
        a = (a + b) / lf_c2;    // 相加平均
        b = lf_sqr(prev * b);   // 相乗平均
        s -= t * (a - prev) * (a - prev);
        t *= lf_c2;
    } while (a > b);

    PI = (a * a * lf_c4) / s;
    return PI;
}

lfloat lf_pow(const lfloat& x, const lfloat& y)
    // 累乗（べき乗）
    // x^y = exp(y * log(x))
{
    if (!x.size && y.size <= 0) {
        // 整数乗のチェックより先にしておかないと不味い。
        throw lf_exception(3003, "0の非整数乗");
    }

    if (!y.size)
        return lf_c1;

    // 整数乗か
    if (!lf_fp(y))
        return lf_pow_int(x, y);

    if (x.size > 0)
        return lf_exp(y * lf_log(x));
    else if (!x)
        return lf_c0;

    throw lf_exception(3002, "負数の非整数乗");
    return lf_c0;  // ダミー
}

lfloat lf_rad(const lfloat& x)
    // 度->ラジアン
{
    return x * lf_pi() / "180";
}

lfloat lf_remainder(const lfloat& x, const lfloat& y)
    // 機能
    //      xをyで割った余り。x - y * ip(x / y)
    // 入力
    //      y != 0
{
    if (!y)
        throw lf_exception(3006, "remainder()の除数が0");

    return x - y * lf_ip(x / y);
}

lfloat lf_rnd()
    // 機能
    //      （擬似）一様乱数
    // 戻り値の範囲
    //      0 <= rnd() < 1
{
    return lfloat(rand()) / (lf_c1 + lfloat(RAND_MAX));
}

lfloat lf_round(const lfloat& x, const lfloat& n)
    // xを小数点以下n + 1桁で四捨五入しn桁まで求める。int(x * 10 ^ n + 0.5) / 10 ^ n
{
    lfloat e;
    e = lf_pow_int("10", lf_int(n + "0.5"));
    return lf_int(x * e + "0.5") / e;
}

lfloat lf_sec(const lfloat& x)
    // xの正割（secant）。単位はラジアン。
{
    return lf_c1 / lf_cos(x);
}

lfloat lf_sgn(const lfloat& x)
    // 機能
    //      符号
    // 戻り値
    //      x < 0のとき-1
    //      x = 0のとき0
    //      x > 0のとき+1
{
    return x.size > 0 ? lf_c1 : (x.size < 0 ? -lf_c1 : lf_c0);
}

lfloat lf_sinh(const lfloat& x)
    // 機能
    //      xの双曲線正弦（hyperbolic sine）。単位はラジアン。
{
    if (lf_abs(x) >= lf_c1) {
        lfloat t(lf_exp(x));
        return (t - lf_c1 / t) / lf_c2;
    }

    lfloat r(x), xx(x * x), c(x), d(lf_c1), i("3.0"), prev;
    do {
        prev = r;
        r += (c *= xx) / (d *= i * (i - lf_c1));
        i += lf_c2;
    } while (r != prev);
    return r;
}

lfloat lf_sqr(const lfloat& x)
    // 機能
    //      xの非負の平方根。
    // 入力
    //      x >= 0
{
    if (x.size < 0)
        throw lf_exception(3005, "sqr()の実引数が負");
    if (!x)
        return lf_c0;

    lfloat s, prev;

    s = x > lf_c1 ? x : lf_c1;
    do {
        prev = s;
        s = (x / s + s) / lf_c2;
    } while (s < prev);

    return prev;
}

lfloat lf_tan(const lfloat& x)
    // 機能
    //      xの正接（tangent）。単位はラジアン。
{
    lfloat a = lf_cos(x);

    if (!a)
        throw lf_exception(11, "tan()の評価");

    return lf_sin(x) / a;
}

lfloat lf_tanh(const lfloat& x)
    // 機能
    //      xの双曲線正接（hyperbolic tangent）。単位はラジアン。
{
    lfloat t(lf_exp(x * lf_c2));
    return (t - lf_c1) / (t + lf_c1);
}

lfloat lf_time()
    // 機能
    //      その日の午前0時0分からの経過秒数。
    // 解説
    //      午前0時0分のtime()は0である。
{
    return "-1";
}

lfloat lf_truncate(const lfloat& x, const lfloat& n)
    // xを小数点以下n + 1桁で切り捨てn桁まで求める。ip(x * 10 ^ n) / 10 ^ n
{
    lfloat e;
    e = lf_pow_int("10", lf_int(n + "0.5"));
    return lf_ip(x * e) / e;
}

lfloat lf_arctan(const lfloat& x)
    // 機能
    //      xの逆正接（arc tangent）。単位はラジアン。
    //      tanθ=xとなるθ
    // 戻り値の範囲
    //      -PI / 2 < atn(x) < PI / 2
{
    if (x < "-1")
        return -lf_pi() / lf_c2 - lf_arctan(lf_c1 / x);
    else if (x > lf_sqr(lf_c2) - lf_c1 && x < lf_sqr(lf_c2) + lf_c1)
        return lf_pi() / lf_c4 - lf_arctan((lf_c1 - x) / (lf_c1 + x));
    else if (x >= lf_sqr(lf_c2) + lf_c1)
        return lf_pi() / lf_c2 - lf_arctan(lf_c1 / x);

    assert(x >= "-1" && x <= "+1");

    lfloat a = lf_eps(lf_c1);
    lfloat b = x / (lf_c1 + lf_sqr(lf_c1 + x * x));
    lfloat c = lf_c1;
    lfloat d;
    do {
        c = lf_c2 * c / (lf_c1 + a);
        d = d = lf_c2 * a * b / (lf_c1 + b * b);
        d = d / (lf_c1 + lf_sqr(lf_c1 - d * d));
        d = (b + d) / (lf_c1 - b * d);
        b = d / (lf_c1 + lf_sqr(lf_c1 + d * d));
        a = lf_c2 * lf_sqr(a) / (lf_c1 + a);
    } while (lf_c1 > a);

    return c * lf_log((lf_c1 + b) / (lf_c1 - b));
}
