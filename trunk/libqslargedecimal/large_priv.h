﻿// -*- mode:c++; coding:utf-8-with-signature -*-

#include "largeint.h"

#ifdef QLD_CONFIG_64
// 2^64 = 18_446_744_073_709_551_616
// Note. BASE * 2 が型に収まらない。繰り上がりに注意.
constexpr ql_s_t BASE = 10'000'000'000'000'000'000ULL;

constexpr int PACK = 19;  // 10^PACK = BASE
#define CFMT  "%019llu"  // 文字列への変換に使用.

#ifdef __SIZEOF_INT128__
// gcc の組み込み型 __int128 だと -pedantic オプションが警告を発する.
// __int128_t のほうが古く, GCC 4.6 / clang 3.1 以降 __int128
// `__SIZEOF_INT128__` マクロの有無で判定してよい.
// See https://stackoverflow.com/questions/16088282/is-there-a-128-bit-integer-in-gcc
//typedef __attribute__((mode(TI))) unsigned int uint128_t;
typedef unsigned __int128 ql_d_t;  // 乗算, 除算の内部で用いる
#endif

#else
/**
 * 基数. 10の冪乗 (ベキ乗) でなければならない.
 * BASE * 2 が型に収まること。2^32 = 4_294_967_296
 */
constexpr ql_s_t BASE = 100;   // DEBUG

constexpr int PACK = 2;  // 10^PACK = BASE
#define CFMT  "%02u"  // 文字列への変換に使用.

// 乗算，除算で用いる。s_t * s_t で桁溢れしてはならない。
typedef uint64_t ql_d_t;

#endif // QLD_CONFIG_64


ql_s_t n_add( ql_s_t u[], int u_size,
                  const ql_s_t v[], int v_size, int ediff );

void n_sub( ql_s_t u[], int u_size,
            const ql_s_t v[], int v_size, int ediff );

ql_s_t n_muladd1(ql_s_t r[], const ql_s_t u[], int u_size, ql_s_t v);

ql_s_t n_bdiv( ql_s_t u[], const ql_s_t v[], int size);


constexpr int compare_uint(const ql_s_t* x, const ql_s_t* y) {
    return *x > *y ? +1 : (*x < *y ? -1 : 0);
}
