/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_CALC_TAB_HH_INCLUDED
# define YY_YY_CALC_TAB_HH_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    IDENTIFIER = 258,              /* IDENTIFIER  */
    VARIABLE_NAME = 259,           /* VARIABLE_NAME  */
    FUNCTION_NAME = 260,           /* FUNCTION_NAME  */
    USER_FUNC = 261,               /* USER_FUNC  */
    UNSIGNED_NUMERIC_CONSTANT = 262, /* UNSIGNED_NUMERIC_CONSTANT  */
    TK_AND = 263,                  /* TK_AND  */
    TK_CASE = 264,                 /* TK_CASE  */
    TK_DEF = 265,                  /* TK_DEF  */
    TK_DIM = 266,                  /* TK_DIM  */
    TK_DO = 267,                   /* TK_DO  */
    TK_END = 268,                  /* TK_END  */
    TK_EXIT = 269,                 /* TK_EXIT  */
    TK_FOR = 270,                  /* TK_FOR  */
    TK_FUNCTION = 271,             /* TK_FUNCTION  */
    TK_STEP = 272,                 /* TK_STEP  */
    TK_THEN = 273,                 /* TK_THEN  */
    TK_TO = 274,                   /* TK_TO  */
    TK_UNTIL = 275,                /* TK_UNTIL  */
    TK_WHILE = 276,                /* TK_WHILE  */
    EOL = 277                      /* EOL  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{

    struct value_t* dval;   // 定数など
    struct CSymbol* sval;
    int sign;


};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int yyparse (void);


#endif /* !YY_YY_CALC_TAB_HH_INCLUDED  */
