// -*- coding:utf-8 -*-

// Bison 3.2: C++ position.hh, stack.hh が廃止.
%require "3.7"

// %error-verbose は非推奨
%define parse.error verbose

// `yacc.c`:       再入可能にするには, "full" にする。(true ではない.)
%define api.pure full

%{ 
/* 電卓プログラム */

#include "ch3hdr2.h"
#include "lfloat.h"

#include <FlexLexer.h>
#include <malloc.h>
#define alloca _alloca

#include <algorithm>
#include <string>
using namespace std;

extern int yylex();
extern char result_buf[];

%}

%union {
    struct value_t* dval;   // 定数など
    struct CSymbol* sval;
    int sign;
}

%token <sval> IDENTIFIER VARIABLE_NAME FUNCTION_NAME USER_FUNC
%token <dval> UNSIGNED_NUMERIC_CONSTANT
%token TK_AND TK_CASE TK_DEF TK_DIM TK_DO TK_END TK_EXIT TK_FOR TK_FUNCTION
%token TK_STEP TK_THEN TK_TO TK_UNTIL TK_WHILE
%token EOL

%type <dval> expression term function_ref muldiv factor
%type <sign> sign

%%

statement_list:             /* 文並び（独自） */
    statement EOL;

statement:                  /* 文 （独自） */
    expression
    {
        $1->lf.get_str(result_buf);
    }
  | let_statement
  | %empty
    ;

let_statement:          /* 数値let文(?.?)    LETを必要ないようにした。 */
    IDENTIFIER '=' expression
    {
        $1->type = 1;       // 変数として登録
        $1->v.lf = $3->lf;
        sl.push_back(*$1);
    }
  | VARIABLE_NAME '=' expression
    {
        $1->v.lf = $3->lf;
    }
  | FUNCTION_NAME '=' expression
    {
        throw lf_exception(4, "関数には代入できない");
    }
  | USER_FUNC '=' expression
    {
        throw lf_exception(4, "関数には代入できない");
    }
    ;

expression:                 /* 数値式(?.?) */
    muldiv
    {
        $$ = $1;
    }
  | '+' muldiv  /* sign muldivとすると，let文とIDENTで衝突 */
    {
        $$ = $2;
    }
  | '-' muldiv
    {
        $$ = new_val_ptr();
        $$->lf = -$2->lf;
    }
  | expression '+' sign muldiv
    {
        $$ = new_val_ptr();
        $$->lf = ($3 > 0) ? ($1->lf + $4->lf) : ($1->lf - $4->lf);
    }
  | expression '-' sign muldiv
    {
        $$ = new_val_ptr();
        $$->lf = ($3 > 0) ? ($1->lf - $4->lf) : ($1->lf + $4->lf);
    }
    ;

muldiv:                     /* 数値項(?.?)      （変更） */
    factor
    {
        $$ = $1;
    }
  | muldiv '*' sign factor
    {
        $$ = new_val_ptr();
        $$->lf = ($3 > 0) ? ($1->lf * $4->lf) : ($1->lf * -$4->lf);
    }
  | muldiv '/' sign factor
    {
        $$ = new_val_ptr();
        if (!$4->lf)
            throw lf_exception(3001, "0除算");
        $$->lf = ($3 > 0) ? ($1->lf / $4->lf) : ($1->lf / -$4->lf);
    }
    ;

factor:
    term
    {
        $$ = $1;
    }
  | term '^' sign factor
    {
        $$ = new_val_ptr();
        $$->lf = ($3 > 0) ? lf_pow($1->lf, $4->lf) : lf_pow($1->lf, -$4->lf);
    }
    ;

term:
    UNSIGNED_NUMERIC_CONSTANT
    {
        $$ = $1;    // lexerでnew()している
    }
  | IDENTIFIER
    {
        throw lf_exception(3101, "初期化されていない変数");
    }
  | VARIABLE_NAME
    {
        $$ = new_val_ptr();
        $$->lf = $1->v.lf;
    }
  | function_ref
    {
        $$ = $1;    // function_refでnew()している
    }
  | '(' expression ')'      { $$ = $2; }
    ;

function_ref:               // 数値関数引用(?.?)
    FUNCTION_NAME
    {
        if ($1->f.param != 0)
            throw lf_exception(3, "引数の数が違う");

        $$ = new_val_ptr();
        $$->lf = ((lfloat(*)())$1->f.ptr1)();
    }
  | FUNCTION_NAME '(' expression ')'
    {
        if ($1->f.param != 1)
            throw lf_exception(3, "引数の数が違う");

        $$ = new_val_ptr();
        $$->lf = ((lfloat(*)(const lfloat&))$1->f.ptr1)($3->lf);
    }
  | FUNCTION_NAME '(' expression ',' expression ')'
    {
        if ($1->f.param != 2)
            throw lf_exception(3, "引数の数が違う");

        $$ = new_val_ptr();
        $$->lf = ((lfloat(*)(const lfloat&, const lfloat&))$1->f.ptr1)($3->lf, $5->lf);
    }
    ;

sign:
    '+' { $$ = 1; }
  | '-' { $$ = -1; }
  | %empty { $$ = 1; }
    ;

%%
