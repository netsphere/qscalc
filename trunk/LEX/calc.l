
%option c++

%option 8bit
/* Unicode 大丈夫かな? */
%option case-insensitive

%option interactive

%option nodefault

%option nounput

/* #define YY_NO_UNISTD_H */
%option nounistd

%option warn

%{ // -*- coding:utf-8 -*-
// 電卓プログラム
// Copyright (c) 1997 Hisashi Horikawa
// JIS X 3003の構文によった。

#include <math.h>

#include "ch3hdr2.h"
#include "calc_tab.h"
#include "lfloat.h"

#include <algorithm>
using namespace std;

%}

unsigned_integer [0-9]+
ui_c  ([0-9]+(\'[0-9]+)*)

%%

    /* 数値 */

({ui_c}(\.)?|{ui_c}?\.{unsigned_integer})(E[-+]?{unsigned_integer})?    {
        yylval.dval = new_val_ptr();
        yylval.dval->lf = yytext;
        return UNSIGNED_NUMERIC_CONSTANT;   // 符号なし数値定数
    }

    /* 予約語 */

    /* 識別子 */

[A-Z][A-Z0-9_]* {
        CSymbol sym;
        //CSymbolList::iterator i;

        sym.name = yytext;
        auto i = find(sl.begin(), sl.end(), sym);
        if ( i == sl.end()) {
            // 未定義
            yylval.sval = new_sym_ptr();
            yylval.sval->name = yytext;
            return IDENTIFIER;
        }

        yylval.sval = &*i; // iterator to pointer
        switch (i->type)
        {
        case 1: // 変数
            return VARIABLE_NAME;
        case 2: // 組み込み関数
            return FUNCTION_NAME;
        case 3: // ユーザー定義関数
            return USER_FUNC;
        default:
            abort();
            break;
        }
    }

    /* その他 */

\n  { return EOL; }

\#[^\n]*     |   /* コメント */
[ \r\t]     ;   /* 空白は無視する */

.       { return yytext[0]; }

%%

yyFlexLexer lexer;
int yylex()
{
    return lexer.yylex();
}
