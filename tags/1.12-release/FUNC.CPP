// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#include "stdafx.h"
#include "QsCalc.h"

#include "ch3hdr2.h"
#include "lfloat.h"
#include <FlexLexer.h>

#include <strstream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////
// 構文解析

char buf[8000];
char result_buf[8000];

extern yyFlexLexer lexer;
extern int yyparse();
void parse_start()
{
    istrstream st(buf, sizeof(buf));
    lexer.switch_streams(&st, NULL);

    try {
        yyparse();
    }
    catch (lf_exception& err) {
		sprintf(result_buf, "ERR%d: %s", err.getCode(), err.what());
    }

    // ポインタをクリアする
    CValPtrList::iterator i;
    for (i = vl.begin(); i != vl.end(); i++)
        delete *i;
    vl.clear();
}

extern "C" int yywrap()
{
    return 1;
}

////////////////////////////////////////////////////////////////////////////
// CSymbol

CSymbol::CSymbol(): type(0)
{
}

CSymbol::~CSymbol()
{
}

////////////////////////////////////////////////////////////////////////////

CSymbolList sl;
CValPtrList vl;
ParamList pl;
ArgList al;
