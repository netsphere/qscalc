// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#include "stdafx.h"

static const char* QsCalcYYERROR = "内部エラー";

#undef YY_FATAL_ERROR
#define YY_FATAL_ERROR(msg) AfxMessageBox(QsCalcYYERROR)

#include "lexyy.cpp"
