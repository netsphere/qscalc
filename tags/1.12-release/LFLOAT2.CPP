// Q's Calc
// Copyright (c) 1997-1999 Hisashi HORIKAWA. All rights reserved.

#include <ctype.h>
#include <assert.h>
#include <algorithm>
using namespace std;

#include "lfloat.h"

////////////////////////////////////////////////////////////////////////////
// lfloat

const lfloat::s_t lfloat::base = 1000000000;
const int lfloat::pack = 9; // 10^pack = base
const char* lfloat::cfmt = "%09d";  // 文字列への変換に使用する

/*
const lfloat::s_t lfloat::base = 10;
const int lfloat::pack = 1;
const char* lfloat::cfmt = "%01d";  // 文字列への変換に使用する
*/

int lfloat::max_prec_10 = 20000;
int lfloat::max_prec = 2224; // lfloat::max_prec_10 / lfloat::pack + 2;
		// 静的変数の初期化の順序は不定。必ず定数にすること。

////////////////////////////////////////////////////////////////////////////
// lfloat 整数演算部

lfloat::s_t lfloat::n_add(s_t u[], const s_t v[], int size)
    // 機能
    //      u += v
    // 入力
    //      u, v: 長さsize
    // 戻り値
    //      桁溢れがあればその値，なければ0
{
    assert(u && v && size > 0);
    s_t reg, k = 0;

    u += size - 1;
    v += size - 1;
    for (int i = size - 1; i >= 0; i--, u--, v--) {
        reg = *u + *v + k;
        *u = reg % base;
        k = reg / base;         // k = 0 or 1
    }

    return k;
}

lfloat::s_t lfloat::n_sub(s_t u[], const s_t v[], int size)
    // 機能
    //      u -= v
    // 入力
    //      u, v: 長さsize
    // 戻り値
    //      0 or 1
    //      引き切れなければその値，引き切れれば0
{
    assert(u && v && size > 0);
    s_t reg, k = 0;

    u += size - 1;
    v += size - 1;
    for (int i = size - 1; i >= 0; i--, u--, v--) {
        reg = base + *u - *v - k;
        *u = reg % base;
        k = 1 - reg / base;
    }

    return k;
}

lfloat::s_t lfloat::n_muladd(s_t r[], const s_t u[], int size, s_t v)
    // 機能
    //      r += u * v
    // 入力
    //      u, r: 長さsize
    // 戻り値
    //      桁溢れがあればその値，なければ0
{
    assert(r && u && size > 0);
    if (!v)
        return 0;

    d_t reg;
    s_t k = 0;

    r += size - 1;
    u += size - 1;
    for (int i = size - 1; i >= 0; i--, r--, u--) {
        reg = d_t(*u) * v + *r + k;
        *r = reg % base;
        k = reg / base;     // 0 <= k < base
    }

    return k;
}

lfloat::s_t lfloat::n_mulsub(s_t u[], const s_t v[], int size, s_t q)
    // 機能
    //      u -= v * q
    // 入力
    //      u, v: 長さsize
    // 戻り値
    //      0 <= 戻り値 < base
    //      (v * q)の桁数が(size + 1)になったとき最上位，そうでないとき0
    //      (v * q)の桁数がsizeでもuから引ききれないときは借りた値。
{
    assert(u && v && size > 0);
    if (!q)
        return 0;

    d_t reg1;
    s_t reg2, k = 0;

    u += size - 1;
    v += size - 1;
    for (int i = size - 1; i >= 0; i--, u--, v--) {
        reg1 = d_t(*v) * q + k;
        reg2 = (base + *u) - (reg1 % base);
        *u = reg2 % base;
        k = (1 - reg2 / base) + reg1 / base;        // 0 <= k < base
    }

    return k;
}

////////////////////////////////////////////////////////////////////////////
// lfloat 実数演算部

lfloat::lfloat(const lfloat& a): size(0), exp(0), frac(NULL)
{
    set(this, &a);
}

lfloat::lfloat(const char* s): size(0), exp(0), frac(NULL)
{
    set_str(this, s);
}

lfloat::lfloat(double a): size(0), exp(0), frac(NULL)
{
    char s[50];
    sprintf(s, "%.15g", a); // doubleだと，この辺が限界
    set_str(this, s);
}

lfloat::~lfloat()
{
    delete [] frac;
}

void lfloat::set_max_prec(int new_prec)
{
    assert(new_prec > 0);
    max_prec_10 = new_prec;
    max_prec = new_prec / pack + 2; 
		// 10進に変換するときに(max_prec_10 + 1)桁目で四捨五入する
		// base進数でのmax_prec桁目を使いたくないので，+2
}

static inline int sign(int a)
{
    return a > 0 ? +1 : (a < 0 ? -1 : 0);
}

double lfloat::get_dbl() const
{
    char* s = new char[abs(size) * pack + 20];  // '20'に意味はない。
    double r = atof(get_str(s));
    delete [] s;
    return r;
}

char* lfloat::get_str(char* s) const
    // lfloat -> str
    //              1.0e-4〜9.9...e+13          -> 123.45
    //  〜9.9...e-5                 1.0e+14     -> 1.2345e+n
{
    assert(s);

    if (!size)
        return strcpy(s, "0");

    int i, j, k = 0;
    char* acc = new char[abs(size) * pack + 1];
                // 一時的な小数部の保存。'+1'はsprintf()の'\0'

    // 1. 小数部を10進に変換する
    for (i = 0; i < abs(size); i++)
        sprintf(acc + i * pack, cfmt, frac[i]);

    // 2. 10進での指数を調べ，小数部の先行する0を除く
    int e10 = exp * pack;
    char* fst = acc;
    while (*fst == '0') {
        fst++; e10--;
    }

    // 3. 丸める
    //      丸めるのは，内部でbase進を使っているのを隠すため
    //      この辺りはround()とほとんど同じ。ただし'0'〜'9'を使う
    int flen = abs(size) * pack - (fst - acc);
    if (flen > max_prec_10) {
        k = fst[flen = max_prec_10] >= '5';
        for (j = max_prec_10 - 1; j >= 0 && k; j--) {
            if (fst[j] == '9') {
                fst[j] = '0'; k = 1; flen--;
            }
            else {
                fst[j]++; k = 0;
            }
        }
    }
    if (k) {
        fst[0] = '1'; flen = 1; e10++;
    }

    // 小数部の長さを求める
    for (i = flen - 1; i >= 0; i--) {
        if (fst[i] == '0')
            flen--;
        else
            break;
    }
    if (i < 0) {
        // 四捨されて，0になっている
        delete [] acc;
        return strcpy(s, "0");
    }

    // 5. 出力
    char* p = s;    // sは戻り値に使う
    if (size < 0)
        *(p++) = '-';

    if (e10 >= -3 && e10 <= +14) {
        // 123.45形式
        if (e10 > 0) {
            // 絶対値1以上
            for (i = 0; i < flen; i++) {
                if (i == e10)
                    *p++ = '.';
                *p++ = fst[i];
                if (!((e10 - i - 1) % 3) && i < e10 - 1)
                    *p++ = '\'';
            }
            for (i = 0; i < (e10 - flen); i++) {
                *p++ = '0';
                if (!((i - e10 + flen + 1) % 3) && i < e10 - flen - 1)
                    *p++ = '\'';
            }
        }
        else {
            // 絶対値1未満
            *(p++) = '0'; *(p++) = '.';
            for (i = 0; i < -e10; i++)
                *(p++) = '0';
            strncpy(p, fst, flen);
            p += flen;
        }
        *p = '\0';
    }
    else {
        // 1.2345e+n形式
        *(p++) = fst[0];
        *(p++) = '.';
        if (flen <= 1)
            *p++ = '0';
        for (i = 1; i < flen; i++)
            *(p++) = fst[i];
        sprintf(p, "e%d", e10 - 1);
    }

    delete [] acc;
    return s;
}

////////////////////////////////////////////////////////////////////////////
// 代入演算子

lfloat* lfloat::set(lfloat* r, const lfloat* u)
    // r = u
{
    assert(r);
    if (r == u)
        return r;

    if (!u || !u->size) {
        r->size = 0;
        r->exp = 0;
        delete [] r->frac;
        r->frac = NULL;
    }
    else {
        if (abs(r->size) < abs(u->size)) {
            delete [] r->frac;
            r->frac = new s_t[abs(u->size)];
        }
        r->size = u->size;
        r->exp = u->exp;
        memcpy(r->frac, u->frac, abs(u->size) * sizeof(s_t));
    }
    return r;
}

lfloat* lfloat::set_str(lfloat* r, const char* s)
    // 入力
    //      ({ui_c}(\.)?|{ui_c}?\.{unsigned_integer})(E[-+]?{unsigned_integer})?    {
{
    assert(r);  // s = NULLもあり。

    // 初期化
    r->size = 0;
    r->exp = 0;
    delete [] r->frac;
    r->frac = NULL;

    if (!s)
        return r;

    const char* start;  // 有意な小数部の先頭
    int i;
    int e = 0;      // 10進での指数
    int si = 0;     // 符号

    // 符号
    if (*s == '-') {
        si = -1; s++;
    }
    else {
        si = +1;
        if (*s == '+')
            s++;
    }

    // 同じ"12345"でも12, 3450のときもあれば1234, 5000となることもある。
    // 初めにbase進数での指数を求めてから，数字列の最初に戻って読む

    // 先行する0を除く
    while (*s && (*s == '0' || *s == ',' || *s == '\''))
        s++;
    if (isdigit(*s) || *s == ',' || *s == '\'') {
        // 000123
        //    ^
        start = s;
        do {
            if (isdigit(*s))
                e++;
        } while (s++, (isdigit(*s) || *s == ',' || *s == '\''));
        if (*s == '.')
            s++;
    }
    else if (*s == '.') {
        while (*(++s) == '0')   // 小数点以下は','は不可
            e--;
        if (!isdigit(*s)) {
            // 0000.0000e
            //          ^
            return set_str(r, NULL);
        }
        start = s;
    }
    else {
        // 0000e
        //     ^
        return set_str(r, NULL);
    }

    // 小数点以下
    while (*s && isdigit(*s))
        s++;

    // 指数
    if (*s == 'e' || *s == 'E') {
        int ei = e + atoi(s + 1);
        __int64 e64 = __int64(e) + _atoi64(s + 1);
        if (e64 != ei)
            throw lf_exception(1001, "オーバーフロー");
        e = ei;
    }

    // base進に変換
    r->exp = e > 0 ? (e - 1) / pack + 1 : e / pack;

    // 小数部
    int flen = strlen(start) / pack + 2;    // "1.2" -> 1, 2000
    s_t* acc = new s_t[flen];
    memset(acc, 0, flen * sizeof(s_t));
    s = start;
    int n = (pack - e % pack) % pack;
    for (s_t* p = acc; ; n = 0, p++) {
                    /* 1998.12.10
                        "1.2"    -> 0001 2000
                        "1.2e-1" -> 1200
                        指数部によって，小数点が桁の途中で出現することがある。
                    */
        for (i = n; i < pack; i++) {
redo:
            if (*s == '.' || *s == ',' || *s == '\'') {
                s++;
                goto redo;
            }
            if (isdigit(*s)) {
                *p = *p * 10 + (*s - '0');
                s++;
            }
            else if (!*s || *s == 'e' || *s == 'E') {
                for (int j = i; j < pack; j++)
                    *p *= 10;   // 数字は上から詰めていく
                goto label2;
            }
            else {
                delete [] acc;
                return set_str(r, NULL);
            }
        }
    }
label2:

    r->round(si, r->exp, acc, flen);
    delete [] acc;

    return r;
}

////////////////////////////////////////////////////////////////////////////
// 算術演算

void lfloat::round(int new_sign, int new_exp, s_t u[], int u_size)
    // 機能
    //      (max_prec + 1)桁目を四捨五入してmax_prec桁目まで求め，*thisに代入する。
    // 入力
    //      u: 長さsize
    //      u[]は，0が先行することもあるし，末尾が0になっていることもある。
{
    assert(new_sign >= -1 && new_sign <= 1 && u && u_size > 0);

    int i, j, k = 0;

    // 先行する0を除く
    s_t* fst = u;
    while (!*fst) {
        fst++; new_exp--;
    }

    // 四捨五入する。
    int flen = u_size - (fst - u);
    if (flen > max_prec) {
        k = fst[flen = max_prec] >= base / 2;
        for (j = max_prec - 1; j >= 0 && k; j--) {
            if (fst[j] == base - 1) {
                fst[j] = 0; k = 1; flen--;  // ここでflenを切り詰めておくと，次の小数部の長さを求めるところが速くなる。
            }
            else {
                fst[j]++; k = 0;
            }
        }
    }
    if (k) {
        fst[0] = 1; flen = 1; new_exp++;
    }

    // 小数部の長さを求める
    for (i = flen - 1; i >= 0; i--) {
        if (!fst[i])
            flen--;
        else
            break;
    }
    if (i < 0) {
        // 四捨されて，0になっている
        size = 0;
        exp = 0;
        delete [] frac;
        frac = NULL;
        return;
    }

    // 値を設定
    if (abs(size) < flen) {
        delete [] frac;
        frac = new s_t[flen];
    }
    size = new_sign * flen;
    exp = new_exp;
    memcpy(frac, fst, flen * sizeof(s_t));
}

lfloat* lfloat::add(lfloat* r, const lfloat* u, const lfloat* v)
{
    const lfloat *up, *vp;  // eu >= evとするため
    int ediff;      // eu - ev
    s_t *acc1, *acc2;
    int acsize;

    // どちらかが0の時
    if (!u->size)
        return set(r, v);
    if (!v->size)
        return set(r, u);

    // u, vの符号が異なるときは減算ルーチンを呼ぶ
    if (sign(u->size) != sign(v->size)) {
        lfloat vn(*v);
        vn.size = -vn.size;
        return sub(r, u, &vn);
    }

    // A2. eu >= evとする
    up = u->exp >= v->exp ? u : v;
    vp = u->exp >= v->exp ? v : u;

    // A4. 指数部の差が十分大きいときは大きい方を答えとする。
    //      0.9999                   0.9999
    //     +0.00009                 +0.000009
    //     --------                 ---------
    //      0.99999 -> 1.000         0.999909 -> 0.9999
    ediff = up->exp - vp->exp;
    if (ediff >= max_prec + 1)
        return set(r, up);

    // 累算器(accumulator)に設定
    //       0.9999
    //      +0.00009999
    //      -----------
    //       0.99999999 -> 1.000
    // acc1: * ********
    // acc2:   ********
    acsize = _MAX(abs(up->size), abs(vp->size) + ediff);
    acc1 = new s_t[acsize + 1];
    acc2 = new s_t[acsize];
    memset(acc1, 0, (acsize + 1) * sizeof(s_t));
    memset(acc2, 0, acsize * sizeof(s_t));
    memcpy(acc1 + 1, up->frac, abs(up->size) * sizeof(s_t));
    memcpy(acc2 + ediff, vp->frac, abs(vp->size) * sizeof(s_t));

    // A6. 加算する。小数点以下第1位から第(prec + 1)桁まで計算する。
    acc1[0] = n_add(acc1 + 1, acc2, acsize);

    r->round(sign(up->size), up->exp + 1, acc1, acsize + 1);
    delete [] acc1;
    delete [] acc2;

    return r;
}

lfloat* lfloat::sub(lfloat* r, const lfloat* u, const lfloat* v)
    // r = u - v
    // add()と似てるが，正規化の手順が異なる
{
    const lfloat *up, *vp;  // |u| >= |v|
    int ediff;      // eu - ev
    s_t *acc1, *acc2;
    int si = 0;

    // どちらかが0のとき
    if (!u->size) {
        set(r, v);
        r->size = -r->size;
        return r;
    }
    if (!v->size)
        return set(r, u);

    // u, vの符号が異なるときは加算ルーチンを呼ぶ
    if (sign(u->size) != sign(v->size)) {
        lfloat vn(*v);
        vn.size = -vn.size;
        return add(r, u, &vn);
    }

    // |u| > |v|とする
    switch (compare(u, v))
    {
    case +1:    // u > v
        up = u->size > 0 ? u : v;   vp = u->size > 0 ? v : u;
        si = +1;
        break;
    case 0:     // u == v
        return set(r, 0);
    case -1:    // u < v;
        up = u->size > 0 ? v : u;   vp = u->size > 0 ? u : v;
        si = -1;
        break;
    default:
        assert(0);
    }

    // 指数の差が十分大きいときは大きい方を解とする
    //       0.1000                  0.1000
    //      -0.000009               -0.0000009
    //      ---------               ----------
    //       0.099991 -> 0.09999     0.0999991 -> 0.1000
    ediff = up->exp - vp->exp;
    if (ediff >= max_prec + 2)
        return set(r, up);

    // 累算器(accumulator)に設定
    //       0.1000
    //      -0.000009999
    //      ------------
    //       0.099990001 -> 0.09999
    // acc1:   *********
    // acc2:   *********
    int acsize = _MAX(abs(up->size), abs(vp->size) + ediff);
    acc1 = new s_t[acsize];
    acc2 = new s_t[acsize];
    memset(acc1, 0, acsize * sizeof(s_t));
    memset(acc2, 0, acsize * sizeof(s_t));
    memcpy(acc1, up->frac, abs(up->size) * sizeof(s_t));
    memcpy(acc2 + ediff, vp->frac, abs(vp->size) * sizeof(s_t));

    // 減算を実行
    s_t rrr = n_sub(acc1, acc2, acsize);
    assert(!rrr);

    r->round(si, up->exp, acc1, acsize);
    delete [] acc1;
    delete [] acc2;

    return r;
}

lfloat* lfloat::mul(lfloat* r, const lfloat* u, const lfloat* v)
{
    int i;

    // どちらかが0のとき
    if (!u->size || !v->size)
        return set(r, 0);

    //       0.9999              0.1000
    //      x0.9999             x0.1000
    //      -----------         --------
    //       0.99980001          0.01000
    // acc:    ********
    int asiz = abs(u->size) + abs(v->size);
    s_t* acc = new s_t[asiz];
    memset(acc, 0, asiz * sizeof(s_t));

    // 小数部
    for (i = abs(v->size) - 1; i >= 0; i--) {
        if (!v->frac[i])
            continue;
        acc[i] += n_muladd(acc + i + 1, u->frac, abs(u->size), v->frac[i]);
    }

    r->round(sign(u->size * v->size), u->exp + v->exp, acc, asiz);
    delete [] acc;

    return r;
}

lfloat::s_t lfloat::bdiv(s_t u[], const s_t v[], int size)
    // 機能
    //      u / v
    // 入力
    //      u: 長さsize + 1
    //      v: 長さsize
    // 出力
    //      u = u % v
    // 戻り値
    //      u / v
{
    assert(u && v && size > 0 && v[0] >= base / 2);

    s_t q;  // 商
    d_t d = d_t(u[0]) * base + u[1];

    if (size == 1) {
        q = d / v[0];               // 真の商
        d_t r = d - d_t(v[0]) * q;  // 余り
        u[0] = r / base;
        u[1] = r % base;
        return q;
    }

    // u, vの上位から商の近似値を求める
    q = u[0] == v[0] ? base - 1 : d / v[0];
    while (d_t(v[1]) * q > (d - d_t(q) * v[0]) * base + u[2])
        q--;

    // ここまででqは，真の商と等しいか，真の商より1大きいかのいずれか。
    // 実際に引けるか試して，真の商か判定する。
    if (u[0] -= n_mulsub(u + 1, v, size, q)) {
        // qは1大きかった
        q--;
        u[0] += n_add(u + 1, v, size);
        assert(!u[0]);
    }

    return q;
}

lfloat* lfloat::div(lfloat* r, const lfloat* u, const lfloat* v)
    // r = u / v
{
    assert(r && u && v);

    if (!v->size)
        throw lf_exception(3001, "0除算");
    if (!u->size)
        return set(r, 0);

    int i;
    s_t *acc1, *acc2, *acc3;

    acc1 = new s_t[max_prec + 2];                   // 商。先行する0の1桁と，丸めのための1桁だけ必要。
    acc2 = new s_t[max_prec + abs(v->size) + 2];    // 被除数，余り。商をn桁求めるのにv.size + n桁必要
    acc3 = new s_t[abs(v->size)];                   // 正規化した除数。
    memset(acc1, 0, (max_prec + 2) * sizeof(s_t));
    memset(acc2, 0, (max_prec + abs(v->size) + 2) * sizeof(s_t));
    memset(acc3, 0, abs(v->size) * sizeof(s_t));

    // 正規化
    s_t d = base / (v->frac[0] + 1);
    acc2[0] = n_muladd(acc2 + 1, u->frac, _MIN(abs(u->size), max_prec + abs(v->size) + 1), d);
    s_t rrr = n_muladd(acc3, v->frac, abs(v->size), d);
    assert(!rrr);

    // 小数部
    for (i = 0; i < max_prec + 2; i++)
        acc1[i] = bdiv(acc2 + i, acc3, abs(v->size));

    r->round(sign(u->size * v->size), u->exp - v->exp + 1, acc1, max_prec + 2);
    delete [] acc1;
    delete [] acc2;
    delete [] acc3;

    return r;
}

lfloat lfloat::operator / (const lfloat& a) const
    // 機能
    //      商を返す。
{
    if (!a)
        throw lf_exception(3001, "0除算");

    lfloat r;
    return *div(&r, this, &a);
}

lfloat& lfloat::operator /= (const lfloat& a)
{
    if (!a)
        throw lf_exception(3001, "0除算");

    return *div(this, this, &a);
}

lfloat lfloat::operator % (const lfloat& a) const
{
    if (!a)
        throw lf_exception(3001, "0除算");

    return *this - a * (*this / a);
}

lfloat& lfloat::operator %= (const lfloat& a)
{
    if (!a)
        throw lf_exception(3001, "0除算");

    return *this = *this - a * (*this / a);
}

////////////////////////////////////////////////////////////////////////////
// 比較演算子

int lfloat::compare(const lfloat* x, const lfloat* y)
    // 戻り値
    //      *x > *y     +1
    //      *x == *y    0
    //      *x < *y     -1
{
    assert(x && y);

    int i;

    // 符号
    if (sign(x->size) > sign(y->size))
        return +1;
    else if (sign(x->size) < sign(y->size))
        return -1;
    else if (!x->size && !y->size)
        return 0;

    assert(x->frac && y->frac);

    // 指数部
    if (x->exp > y->exp)
        return sign(x->size);
    else if (x->exp < y->exp)
        return -sign(x->size);

    // 小数部
    for (i = 0; i < _MIN(abs(x->size), abs(y->size)); i++) {
        if (x->frac[i] > y->frac[i])
            return sign(x->size);
        else if (x->frac[i] < y->frac[i])
            return -sign(x->size);
    }
    if (abs(x->size) > abs(y->size))
        return sign(x->size);
    else if (abs(x->size) < abs(y->size))
        return -sign(x->size);

    return 0;
}

lfloat lfloat::operator ++ (int)
{
    lfloat r = *this;
    ++*this;
    return r;
}

lfloat lfloat::operator -- (int)
{
    lfloat r = *this;
    --*this;
    return r;
}

lfloat& lfloat::operator ++()
{
    *this = *this + "1";
    return *this;
}

lfloat& lfloat::operator --()
{
    *this = *this - "1";
    return *this;
}

////////////////////////////////////////////////////////////////////////////
// lf_exception

lf_exception::lf_exception(int n, const char* msg_): code(n), msg(msg_)
{
}

int lf_exception::getCode() const
{
    return code;
}

const char* lf_exception::what() const
{
    return msg.c_str();
}
